package CDK::MascotCSV::File;

use strict;
use warnings;
use Symbol qw/gensym/; 
use Text::CSV_XS ; 
use CDK::Object; 
use CDK::MascotCSV::Indexes qw/:all/; 
use CDK::Mascot::ProteinHitListGroup;
use CDK::Mascot::ProteinHitList; 
use CDK::Mascot::ProteinHit; 
use CDK::Mascot::PeptideHitList; 
use CDK::Mascot::PeptideHit; 
use CDK::Mascot::GenbankMap; 

our  @ISA = qw/CDK::Object/;
our $VERSION = '0.01';
our $PKG = 'CDK::Mascot::File'; 
our $DEBUG = 0; 

sub DEBUG { if ($DEBUG) { warn join "\n", @_} } 
sub DEBUG_ON { $DEBUG=1 }
sub DEBUG_OFF { $DEBUG=0 }  

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my CDK::MascotCSV::File $self = CDK::Object->new(); 
   bless $self, $class; 
   $self->{'csv'} = Text::CSV_XS->new(); 
   if (@_){ 
	$self->file(@_); 
   }
   return $self; 
}


sub file { 
   my $self = shift; 
   $self->{'file'} = $_[0] if @_; 
   return $self->{'file'}; 
}
sub data { 
   my $self = shift; 
   $self->parse() unless ref($self->{'MascotData'}); 
   return $self->{'MascotData'}; 
}

sub genbank_map { 
	my $self = shift; 
	return $self->{'GenbankMap'}; 
}

sub parse { 
   my $self = shift; 
   my $file = $self->file();
  
   #data is the data in the file, no header information by joels request. 
   #data is a list of proteinHitLists, where each proteinHitList is a list of ProteinHits, 
   #one ProteinHit per genbank accessions number. 
   my $genbankMap 	= CDK::Mascot::GenbankMap->new();  
   my $proteinGroups	= CDK::Mascot::ProteinHitListGroup->new(); 
   my $phl  		= CDK::Mascot::ProteinHitList->new(); 
   my $proHit 		= CDK::Mascot::ProteinHit->new(); 
   my $protHit 		= {};
   my $phn  		= -1; #protein hit number; 
   
   #this is a unique symbol being used as filehandle. 
   #the file is the mascot data in comma separated value format. 
   my $fh = gensym(); 
   open ($fh, $file) or die "$PKG: parse can't open file $file\n"; 

   #we don't care about all the header information at this point in Joels analysis, 
   #so we skip until we hit the line that says prot_hit_num.  This is the header line. 
   #the data follows after this. 
    do { $_=<$fh>  } until /prot_hit_num/; 
   #now we parse the file by using the CSV_XS module to convert each line
   #into an array reference, where the indexes represent the columns in the file. 
   #I've imported variables for using as indexes from CDK::MascotCSV::Indexes
   while( my $col_ref = $self->{'csv'}->getline($fh)){
   	#So, if we have a gi identifier, we are starting a new protein hit, 
	#save the old data, if and only if, we have old data. We know this
	#by checking the old data for a gi identifier
	if ($col_ref->[$prot_acc]=~/^gi/){ 
		DEBUG("$PKG: starting new genbankID : $col_ref->[$prot_acc]\n"); 
		#this is where we actually do the check of the old data for a gi identifier. 
		if ($proHit->hasProp('Acc') ){
			#we must have a gi identifier in out last protHit object, 
			#so we can go ahead and save it in our data structures for storing in self
			#at the end of this subroutine. 
			if ($proHit->get('Hit_num') != $phn ) { 
				# the hit number of this protein is not the same as 
				# the hit numbers stored in $phn
				# so we add the protein hit list ($phl) to groups, 
				# update the hit number in $phn, 
				# create a new protein hit list, 
				# add the protein data to the genbank object
				# add the protein data to the protein hit list we just created
				$proteinGroups->add($phl) if $phl->count > 0;
				$phn = $proHit->get('Hit_num'); 
				$phl = CDK::Mascot::ProteinHitList->new(); 
				$genbankMap->set($proHit->get('Acc'), $proHit); 
				$phl->add($proHit); 
			}else { 
				#other wise, this protein has the same hit number as 
				#the previous protein data, so just add it to this 
				#protein hit list
				$phl->add($proHit); 
				#THIS is a hack to see if the NO NOES in joels output are from this brilliant piece of decision making
				unless ($genbankMap->has_genbank_id($proHit->get('Acc'))) { 
					$genbankMap->set($proHit->get('Acc'), $proHit); 
				} 
			}
			#now we clear out the protein object before continuing on to process
			#the peptide data supporting the protein hit 
			$protHit = {}; 
			$proHit = CDK::Mascot::ProteinHit->new(); 
		}
		# The $prot_hit_num .. $prot_seq is a range, the variables are numbers, indexes into the col_refs array.  
		# These variables are imported from CDK::MascotCSV::Indexes. All peptides share this information.
		# This is a hash slice in references, explicit syntax, hash analogue of assigning range in array. 
		#
		# this is protein data, read only if the line contains an accession number. 
		@{$protHit}{qw(hit_num acc desc score mass matches cover len tax_str tax_id seq)} = @{ $col_ref }[$prot_hit_num .. $prot_seq];
		
		#HACK ... can autoload methods with capital letter in proHit object 
		foreach (keys %{ $protHit }) { 
			$proHit->set( ucfirst $_ => $protHit->{$_} ); 
		}	
		# we create a new list to hold the peptide information here, when we encounter
		# a genbank identifier
		$proHit->set("PeptideData"=>CDK::Mascot::PeptideHitList->new()); 
		#CDK HACK : adding in modified peptide property
		$proHit->set("ModifiedPeptideData"=>CDK::Mascot::PeptideHitList->new() ) ; 
	} # END IF BLOCK -- The following lines of the while block run for all lines in the file
		my $peptide_datum = {}; 
		my $peptideHit	 = CDK::Mascot::PeptideHit->new(); 
		my $modified_peptideHit = CDK::Mascot::PeptideHit->new(); #might not need at all, but here it is 
		   # incase we ever want to get from the peptide to the protein, we 
		   # are storing a reference to the protein here
		   $peptideHit->set('ProteinHit' => $proHit); 
		   $modified_peptideHit->set('ProteinHit'=> $proHit); 

		#this pulls out the peptide information part of the line parsed into $col_ref
		@{ $peptide_datum }{ qw (query rank isbold exp_mz exp_mr exp_z 
					calc_mr delta miss score homol
					ident expect res_before seq 
					res_after var_mod var_mod_post ) } = @{ $col_ref }[$pep_query .. $pep_var_mod_pos]; 
		# migrate the data into the peptide hit object
		foreach  (keys %{ $peptide_datum } ){ 
			my $key = ucfirst $_; 
			$peptideHit->set(  $key => $peptide_datum->{$_} );
			$modified_peptideHit->set($key => $peptide_datum->{$_});
		}
		if ($modified_peptideHit->Var_mod and $modified_peptideHit->Var_mod gt "") { #we have a modification, continue 
			my $seq = join "_", ($modified_peptideHit->Seq, $modified_peptideHit->Var_mod); 
			$modified_peptideHit->set("Seq"=>$seq); 
		} 
			
		#get the peptide hit list from the protein 
		#and add the peptide to the peptide hit list
		my $pepHL = $proHit->get("PeptideData");
		$pepHL->add($peptideHit); 
		if ( $modified_peptideHit->Var_mod and  $modified_peptideHit->Var_mod gt "" ) { 
			my $modPepHL = $proHit->get("ModifiedPeptideData"); 
			$modPepHL->add($modified_peptideHit); 
		}
	

   } #END OF WHILE LOOP
	
   #LAST PROTEIN HIT
   $phl->add($proHit); 
   $proteinGroups->add($phl); 
   $self->{'MascotData'} = $proteinGroups; 
   $self->{'GenbankMap'} = $genbankMap; 
}

1; # IT TRUE ! ALWAYS TRUE.
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::MascotCSV::File - Perl extension for blah blah blah

=head1 SYNOPSIS
  use Strict; 
  use CDK::MascotCSV::File;

  my $desired_organism = qr/melanogaster/io;   
  my $mf = CDK::MascotCSV::File->new(); 
  $mf->file("path/to/file/file.csv"); 
  $mf->parse(); 
  my $data = $mf->data(); 
  my $gbMap = $mf->genbank_map(); 
  for( my $itr=$data->getIterator; $itr->hasNext();){
	my $proteinHitList = $itr->next(); 
	for(my $itr = $proteinHitList->getIterator(); $itr->next();){
		my $protein = $itr->next(); 
		next unless $protein->Tax_str=~/melanogaster/i; 
		print join "\t"	,( $protein->Hit_num, 
				   $protein->Acc, 
				   $protein->Desc, 
				   $protein->Len, 
				   $protein->Score, 
				   $protein->Tax_str, 
				   $protein->Seq, 
				   $protein->Tax_id, 
				   $protein->Mass, 
				   $protein->Matches ) ; 
		my $unique_peptide_count = $protein->unique_peptide_count(); 
		print ($protein->Acc, " has $unique_peptide_count unique peptides\n");
		print join "\n", $protein->unique_peptides();
		my $peptideList = $protein->get('PeptideData'); 
		my $modifiedPeptideList = $protein->get("ModifiedPeptideData"); 
		foreach my $peptide ( $peptideList->getArray() ) { 
			my $seq 	= $peptide->Seq;
			my $mod 	= $peptide->Var_mod; 
			my $score 	= $peptide->Score; 
			print join "\n", ("seq:$seq", "mod:$mod", "score:$score");			
		}
	}
   }
   my @genbank_ids = $gbMap->genbank_id_list->getArray();
   if ($gbMap->has_genbank_id("gi|99999"){
	#DO SOMETHING 
	my $protein = $gbMap->get($genbank_ids[0]); 
	print $protein->Desc, "\n"; 
	print "this sample contains the stuff\n";
   }else { 
	#NOT IN HERE 
	warn "Can't find that in here \n";
   }
 
=head1 DESCRIPTION

Takes a MascotCSV file and converts it into two data structures. 

1) ProteinHitGroups - A List of ProteinHitLists, one per protein group in the csv file. 
2) Genbank ID Map   - Maps genbank identifiers to CDK::Mascot::ProteinHit objects

=head2 EXPORT

None by default.



=head1 SEE ALSO

	CDK::List 
	CDK::Mascot::PeptideHit 
	CDK::Mascot::ProteinHit 
	CDK::Mascot::ProteinHitList 
	CDK::Mascot::PeptideHitList 
	CDK::Mascot::ProteinHitListGroup 
	CDK::MascotCSV::Indexes 
	CDK::Object 

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
