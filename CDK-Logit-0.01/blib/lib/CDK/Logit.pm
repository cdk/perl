package CDK::Logit;

use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use CDK::Logit ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( logit tag priority stderr ) ] ); 
	

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
our $TAG = 'CDK::Logit'; 
our $PRIORITY = 'user.test'; 
our $STANDARD_ERROR = undef; 
sub priority { $PRIORITY = shift} 
sub tag { $TAG = shift } 
sub stderr { if (@_) { 	$STANDARD_ERROR = '-s ';
	     }else { 
			$STANDARD_ERROR = undef; 
	     }
}
sub logit { 
  `logger $STANDARD_ERROR -t $TAG -p $PRIORITY \"$_\"` while($_=shift @_); 
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Logit - log to OS X console

=head1 SYNOPSIS

  use CDK::Logit qw/ tag priority logit all/;
  tag("TESTING"); 
  priority("daemon.test"); 
  logit("this is a test message"); 

=head1 DESCRIPTION

instead of having to implement this every thing I write, now I can 
get debugging information to the console for scripts that don't run 
from the command line . . . 

Blah blah blah.

=head2 EXPORT

None by default.

We don't pollute your name space. 
CDK::Logit::tag will work fine if you have a tag sub in your code 

blah blah blah 

=head1 SEE ALSO


=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.

=cut
