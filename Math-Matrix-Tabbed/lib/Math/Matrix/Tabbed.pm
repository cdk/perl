package Math::Matrix::Tabbed;

use strict;
use warnings;
use Math::Matrix; 

our @ISA = qw(Math::Matrix);

sub new () { 
  my $class = shift; 
  my $self = Math::Matrix->new(@_); 
  return  bless $self, $class; 
}
sub as_string {
    my $self = shift;
    my $out = "";
    for my $row (@{$self}) {
        for my $col (@{$row}) {
            $out = $out . "\t" . $col;
        }
        $out = $out . sprintf "\n";
    }
    $out;
}
