package CDK::Mascot::Help ; 
=head1 USAGE NOTES 

=head1 perldoc PACKAGENAME 
  
  Read the documentation that comes with the modules.  
  It isn't much, but it is generally there for your benefit. 
  
  perldoc CDK::MascotCSV::Indexes
  perldoc CDK::MascotCSV::File
  perldoc CDK::Mascot::ProteinHitLIstGroup
  perldoc CDK::Mascot::ProteinHitList
  perldoc CDK::Mascot::ProteinHit
  perldoc CDK::Mascot::PeptideHitList
  perldoc CDK::Mascot::PeptideHit 
 
=head1 CDK::MascotCSV::Indexes 
	
	This is mostly for the developer of the CDK::MascotCSV::File module. 
	It provides readonly variables with the same names as the headers of the mascot file. 
	Using this module lets the CDK::MascotCSV::File developer refer to things like $ref->[$seq] as
	opposed to $ref->['11']. 

=head1 CDK::MascotCSV::File  
	
	This class is your entry point for dealing with the comma separated values that come from the
	website Joel uses to analyse his mass spec stuff.  
	
	It provides access to the name of the file it is currently using, a map of genbank identifiers
	to CDK::Mascot::ProteinHit objects, and a data structure that represents the protein hit groups
	contained in the Mascot file. 
	
	Typically the object is created and loaded like this
	my $mf = CDK::MascotCSV::File->new(); 
	$mf->file("/path/to/JoelsData/file.csv"); 
	$mf->parse(); 
	
	now you can retrieve the data from the file 
	my $proteinHitListGroup 		= $mf->data; 
	my $genbankMap  			= $mf->genbank_map; 
	my $idList 				= $genbankMap->genbank_id_list(); 
	my $itr 				= $idList->getIterator; 
	my @ids 				= $idList->getArray(); 
	if ($genbankMap->has_genbank_id("gi|9999") ) { 
		# IT DOES, SO WHAT ARE YOU GONNA DO ABOUT IT ? 
	}else {
		#NO NO NO 
	}
	
=head1 CDK::Mascot::ProteinHitListGroup 
 	
 	A simple CDK::List of ProteinHitList objects -- generated in CDK::MascotCSV::File::parse()
 	
 	my $data = $mf->data(); #$data is a CDK::Mascot::ProteinHitListGroup
 	for(my $itr = $data->getIterator; $itr->hasNext()){
 		my $proteinHitList = $itr->next(); 
 		do something with the proteinHitList
 	}
 	
=head1 CDK::Mascot::ProteinHitList 
 	
 	A simple CDK::List of ProteinHit objects -- generated in CDK::MascotCSV::File::parse()
 	$itr = $proteinHitList->getIterator(); 
 	while($itr->hasNext()){ 
 		my $proteinHit = $itr->next(); 
 	  	#do something with proteinHit
 	}
 	@proteins = $proteinHitList->getArray(); 
 	 foreach (@proteins) { 
 	 	print join "\t", ($_->Acc, $_->unique_peptide_count, $_->Desc); 
 	 }

=head1 CDK::Mascot::ProteinHit 
 	
 	An extension of CDK::Object for handling data related to protein hits in Joels Mascot data
	Each protein hit encapsulates a PeptideHitList containing PeptideHits worth of data related
	to the ProteinHit represented by the object
	
	foreach my $p (@proteins ) { 
		next unless $p->Score 					> $MIN_SCORE ;
		next unless $p->Tax_str 				=~ /melanogaster/; 
		next unless $p->unique_peptide_count 			> $MIN_PEPTIDE_COUNT; 
		print join "\t", ( $p->Acc, $p->Desc, $p->Score, 
				   $p->Len, $p->unique_peptide_count ) ; 
     } 
	
=head1 CDK::Mascot:PeptideHitList 
 	
 	A simple CDK::List of PeptideHits
 	my $peptideHitList = $protein->PeptideData; 
 
=head1  CDK::Mascot::PeptideHit
 	
 	An extension of CDK::Object for handling peptide data for each protein hit. 
 	my @peptides = $peptideHitList->getArray(); 
 	forach my $pep (@peptides) { 
 		print join "\t", ($pep->ProteinHit->Acc, $pep->ProteinHit->Desc, $pep->ProteinHit->Score, $pep->Score, $pep->Seq, $pep->Mod_var); 
     }
     
=head1 CDK:: List 
 	
 	An object that provides very simple list services. 
 	$list->add('thing'); # adds 'thing' to $list
 	
 	#A list can be iterated over 
 	$itr = $list->getIterator();  # get an iterator for the list
 	while($Itr->hasNext()){
 		my $thing = $itr->next(); 
 	} 
 	
 	#the whole list can be returned as an array
 	my @array = $list->getArray(); 
 	my $number = $list->count;
 
=head1 CDK::Iterator 
	
	my $itr = $List->getIterator(); 
	if ($itr->hasNext() ) { 
		my $thing = $Itr->next(); 
	}else { 
		warn "iterator is empty\n"
 	}
 	
 	
=head1  CDK::Object
  	
  	My generic hash based Object for storing simple key=value relationships
  	Currently very much a work in progress
  	$obj=CDK::Object->new(); 
  	$obj->set('Name' 		=> "Jim"); 
  	$obj->set('Address'		=> { 'Str'=>"123 Elm", 
  						'St' =>'CA', 
  						 'zip'=>'823541' }, 
  			  ); 
  	my $name = $obj->get('Name'); 
  	    $name = $obj->Name;  #same thing
  	#change the name
  	$obj->Name('Charlie'); 
  	my @prop = $obj->properties(); 
  	my $prop_Itr = $obj->propertiesList->getIterator(); 

=cut



1; # IT TRUE
