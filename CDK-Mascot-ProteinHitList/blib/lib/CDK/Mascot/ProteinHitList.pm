package CDK::Mascot::ProteinHitList;

use strict;
use warnings;
use CDK::List; 

our @ISA = qw(CDK::List);
our $PKG = 'CDK::Mascot::ProteinHitList'; 
our $VERSION = '0.01';

sub add { 
   my $self = shift; 
   my $protein = shift; 
   if ($protein->isa("CDK::Mascot::ProteinHit")){
	$self->SUPER::add($protein); 
   }else { 
	warn "$PKG: Expecting a CDK::ProteinHit in sub add, found $protein\n"; 
   }
}



1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::ProteinHitList - Module for handling a list of Protein Hits

=head1 SYNOPSIS

  use CDK::Mascot::ProteinHitList;
  my $phl = CDK::Mascot::ProteinHitList->new(); 
  #add some proteins to the list;
  $phl->add($proteinHit); 
  $phl->add($proteinHit2); 
  
  #getArray returns the phl contents as array
  @proteins = $phl->getArray(); 
 
  #can also use Iterator to access contents 
  for(my $itr = $phl->getIterator();$itr->hasNext(); ){
 	my $protein = $itr->next(); 
	print $protein->Score; 
  }
 

=head1 DESCRIPTION

convenience subclass of CDK::List, adds in checking for ProteinHit object before calling add in superclass. 

=head2 EXPORT

none. 


=head1 SEE ALSO

CDK::List, CDK::Iterator, CDK::Mascot::ProteinHit, CDK::Mascot::PeptideHitList, CDK::Mascot::PeptideHit, CDK::Mascot::ProteinHitGroup 

CDK::Mascot::ProteinHit for information on using the protein objects 

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
