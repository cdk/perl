package CDK::MascotCSV::Indexes;

use strict;
use Readonly; 
require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use CDK::MascotCSV::Indexes ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	$prot_hit_num
	$prot_acc
	$prot_desc
	$prot_score
	$prot_mass
	$prot_matches
	$prot_cover
	$prot_len
	$prot_tax_str
	$prot_tax_id
	$prot_seq
	$pep_query
	$pep_rank
	$pep_isbold
	$pep_exp_mz
	$pep_exp_mr
	$pep_exp_z
	$pep_calc_mr
	$pep_delta
	$pep_miss
	$pep_score
	$pep_homol
	$pep_ident
	$pep_expect
	$pep_res_before
	$pep_seq
	$pep_res_after
	$pep_var_mod
	$pep_var_mod_pos
	index_map
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
Readonly our $prot_hit_num	=>	0 ;
Readonly our $prot_acc		=>	1 ;
Readonly our $prot_desc		=>	2 ;
Readonly our $prot_score	=>	3 ;
Readonly our $prot_mass		=>	4 ;
Readonly our $prot_matches	=>	5 ;
Readonly our $prot_cover	=>	6 ;
Readonly our $prot_len		=>	7 ;
Readonly our $prot_tax_str	=>	8 ;
Readonly our $prot_tax_id	=>	9 ;
Readonly our $prot_seq		=>	10 ;
Readonly our $pep_query		=>	11 ;
Readonly our $pep_rank		=>	12 ;
Readonly our $pep_isbold	=>	13 ;
Readonly our $pep_exp_mz	=>	14 ;
Readonly our $pep_exp_mr	=>	15 ;
Readonly our $pep_exp_z		=>	16 ;
Readonly our $pep_calc_mr	=>	17 ;
Readonly our $pep_delta		=>	18 ;
Readonly our $pep_miss		=>	19 ;
Readonly our $pep_score		=>	20 ;
Readonly our $pep_homol		=>	21 ;
Readonly our $pep_ident		=>	22 ;
Readonly our $pep_expect	=>	23 ;
Readonly our $pep_res_before	=>	24 ;
Readonly our $pep_seq		=>	25 ;
Readonly our $pep_res_after	=>	26 ;
Readonly our $pep_var_mod	=>	27 ;
Readonly our $pep_var_mod_pos	=>	28 ;

Readonly our %map => (
	prot_hit_num	=>	0 ,
	prot_acc	=>	1 ,
	prot_desc	=>	2 ,
	prot_score	=>	3 ,
	prot_mass	=>	4 ,
	prot_matches	=>	5 ,
	prot_cover	=>	6 ,
	prot_len	=>	7 ,
	prot_tax_str	=>	8 ,
	prot_tax_id	=>	9 ,
	prot_seq	=>	10 ,
	pep_query	=>	11 ,
	pep_rank	=>	12 ,
	pep_isbold	=>	13 ,
	pep_exp_mz	=>	14 ,
	pep_exp_mr	=>	15 ,
	pep_exp_z	=>	16 ,
	pep_calc_mr	=>	17 ,
	pep_delta	=>	18 ,
	pep_miss	=>	19 ,
	pep_score	=>	20 ,
	pep_homol	=>	21 ,
	pep_ident	=>	22 ,
	pep_expect	=>	23 ,
	pep_res_before	=>	24 ,
	pep_seq		=>	25 ,
	pep_res_after	=>	26 ,
	pep_var_mod	=>	27 ,
	pep_var_mod_pos	=>	28  ); 
Readonly our %reverse_map =>( reverse %map ) ;
sub index_map {return %map } 
sub reverse_map { return %reverse_map } 
sub map_key { return wantarray ? keys %map : \( keys %map )}


1;
__END__

=head1 NAME

CDK::MascotCSV::Indexes - Perl extension for getting index in massive mascot csv file

=head1 SYNOPSIS

  use CDK::MascotCSV::Indexes qw/all/;
  @indexes = index_map(); 
  print join "\n", @indexes; 
  
=head1 DESCRIPTION
  maps indexes to readonly scalars
  a map of the index_names to numbers is available with index_map(). 
  a list of the keys in the map is available from map_key()


=head2 EXPORT

None by default.



=head1 SEE ALSO


=head1 AUTHOR

cameron kennedy E<lt>cameron.kennedy@gmail.comE<gt>

made from 100% recycled electrons.  No animals were
harmed during the development and testing of this module.  Not sold in
stores!

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
