package CDK::Mascot::ProteinHit;

use strict;
#use warnings;
use CDK::Object; 

our @ISA = qw(CDK::Object);

our $VERSION = '0.01';
our $PKG = 'CDK::ProteinHit'; 
our $DEBUG = 0; 
sub DEBUG_ON { $DEBUG=1}
sub DEBUG_OFF { $DEBUG =0} 
sub DEBUG { warn "$PKG: @_\n" if $DEBUG} 

sub unique_peptide_count { 
	my $self = shift; 
	$self->_calculate_unique_peptides() unless $self->hasProp('UniquePeptideCount'); 
	return $self->get('UniquePeptideCount'); 
}
sub unique_peptides { 
	my $self = shift; 
	$self->_calculate_unique_peptides() unless $self->hasProp("UniquePeptideList"); 
	my @uniq_peps = $self->get('UniquePeptideList')->getArray();
	return wantarray ? @uniq_peps : \@uniq_peps; 
}
sub unique_peptide_list { 
	my $self = shift; 
	$self->_calculate_unique_peptides unless $self->hasProp("UniquePeptideList"); 
	return $self->get("UniquePeptideList"); 
}
sub unique_peptide_iterator { 
	my $self = shift; 
	$self->_calculate_unique_peptides unless $self->hasProp("UniquePeptideList"); 
	return $self->get('UniquePeptideList')->getIterator(); 
}
sub unique_modified_peptides { 
	my $self = shift; 
   	$self->_calculate_unique_peptides unless $self->hasProp("UniquePeptideList"); 
	my @ret = grep /_/, $self->get("UniquePeptideList")->getArray(); 
	return wantarray ? @ret : \@ret; 
} 
sub _calculate_unique_peptides { 
   my $self = shift; 
   my $pepL = $self->get('PeptideData'); 
   my %uniq; 
   my $caller = caller(); 
   DEBUG ("$caller called me to calculate_uniques");
   for(my $itr = $pepL->getIterator(); $itr->hasNext(); ){
	my $peptide = $itr->next(); 
	DEBUG("peptide: $peptide"); 
	my $mod = $peptide->hasProp('Var_mod') ? $peptide->get('Var_mod') : 'UnModified' ;	
	my $key = join "_", ( $peptide->get('Seq'), $mod );
	DEBUG("key=$key\n"); 
 	$uniq{$key}++; 
   }
   my $uniq = scalar keys %uniq; 
   $self->set('UniquePeptideList', CDK::List->new( ( keys %uniq ) )); 	
   $self->set('UniquePeptideCount', $uniq); 
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::ProteinHit - Perl extension stores the protein data for a single accession numbers peptide hits. 

=head1 SYNOPSIS

 Normally not created directly, but as part of the data structure used in CDK::MascotCSV::File.  

 my $mf = CDK::MascotCSV::File->new(); 
   $mf->file('/path/to/file/file.csv'); 
   $mf->parse(); 
   my $proteinHitGroups = $mf->data; 
   foreach my $listOfProteinHits ($proteinHitGroups->getArray()){
	foreach my $protein ( $listOfProteinHits->getArray() ) { 
		my $prot_hit_group_num 	= $protein->Hit_num; 
		my $score  		= $protein->Score; 
		my $length 		= $protein->Len; 
		my $mass 		= $protein->Mass;
		my $matches		= $protein->Matches; 
		my $description		= $protein->Desc; 
		my $genbank_id 		= $protein->Accession; 
		my $cover 		= $protein->Cover; 
		my $tax_str		= $protein->Tax_str; 
		my $tax_id		= $protein->Tax_id;
		my $peptideHitList	= $protein->PeptideData; 
		   my @peptides 	= $peptideHitList->getArray(); 
		   my $pep 		= shift @peptides; 
		   my $pep_seq		=  $pep->Seq; 
		   my $mod 		= $pep->Mod_var; 
		my $uniq_pep_count 	= $protein->unique_peptide_count(); 
		my @uniq_peptides	= $protein->unique_peptides(); 
		my $uniq_peptide_list	= $protein->unique_peptide_list(); 
		my $uniq_peptide_itr	= $protein->unique_peptide_iterator(); 
	}
   }
   print ("done parsing file: ", $mf->file, "\n"); 

=head1 DESCRIPTION

CDK::Mascot::ProteinHit - container for data related to protein information from a mascot file. 

=head2 EXPORT

=head1 SEE ALSO

CDK::MascotCSV::File, CDK::MascotCSV::Indexes, CDK::Mascot::ProteinHitListGroup, CDK::Mascot::PeptideList, CDK::Mascot::Peptide, CDK::Mascot::ProteinHitList


=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>


=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
