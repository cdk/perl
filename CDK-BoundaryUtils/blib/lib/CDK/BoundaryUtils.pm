package CDK::BoundaryUtils;

use strict;
use warnings;
require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use CDK::BoundaryUtils ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
    splitX
    split2L
    split2R
    split3L
    split3R	
    setMap
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
our $map = {   'X'      =>  22030326,
                '2L'    =>  22000975,
                '2R'    =>  1285689,
                '3L'    =>  22955576,
                '3R'    =>  378656  } ; 


sub setMap { 
    $map = shift if @_; 
} 

sub splitX ($){ 
    my $seq = shift @_; 
    my $chrX = substr($seq, 0, $map->{'X'}); 
    my $hChr = substr($seq, $map->{'X'}); 
    my @return = ($chrX, $hChr); 
    return wantarray ? @return : \@return; 
} 

sub split2L ($) { 
    my$seq = shift; 
    my $chr2L = substr($seq, 0, $map->{'2L'}); 
    my $hChr2L = substr($seq, $map->{'2L'}); 
    my @return = ($chr2L, $hChr2L); 
    return wantarray ? @return : \@return; 
} 
sub split2R ($){
    my $seq = shift;
    my $chr2R = substr($seq, $map->{'2R'}); 
    my $hChr2R = substr($seq, 0, $map->{'2R'}); 
    my @return = ($chr2R, $hChr2R);
    return wantarray ? @return : \@return; 
}
sub split3L ($) { 
    my$seq = shift; 
    my $chr3L = substr($seq, 0, $map->{'3L'}); 
    my $hChr3L = substr($seq, $map->{'3L'}); 
    my @return = ($chr3L, $hChr3L); 
    return wantarray ? @return : \@return; 
} 
sub split3R ($){
    my $seq = shift;
    my $chr3R = substr($seq, $map->{'3R'}); 
    my $hChr3R = substr($seq, 0, $map->{'3R'}); 
    my @return = ($chr3R, $hChr3R);
    return wantarray ? @return : \@return; 
}
1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::BoundaryUtils - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::BoundaryUtils;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::BoundaryUtils, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
