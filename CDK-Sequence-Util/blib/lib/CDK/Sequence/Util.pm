package CDK::Sequence::Util;

use strict;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

our @EXPORT_OK = qw(comp compliment revcomp alphamin);

our @EXPORT = qw(
	
);

our $VERSION = '0.01';

sub compliment { (my $seq=shift)=~tr/ATGCatgc/TACGtac/;$seq }
sub comp       {return compliment(@_)}
sub revcomp    {return ($_=reverse(compliment(shift @_)))}
sub alphamin {
   my $xmer = shift; 
   my $l = length $xmer; 
   my @vers = $xmer; 
   for (my $i=1;$i<$l;$i++){
      my $kmer = substr($xmer,$i) . substr($xmer, 0,$i);  
      push @vers, $kmer; 
   }
   if (scalar @vers == 1){
      return $vers[0];
   }else { 
      @vers = sort {$a cmp $b} @vers; 
      warn "received : $xmer\treturning $vers[0]\n";
      return $vers[0]; 
   }
}

1;
__END__

=head1 NAME

CDK::Sequence::Util - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Sequence::Util;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::Sequence::Util, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron kennedy, E<lt>cameron@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
