package CDK::TandemRepeatThing::Report;
use strict; 
use Symbol qw/gensym/;
use CDK::Sequence::Util qw/revcomp compliment/; 
our $PKG = 'CDK::TandemRepeatThing::Report';
our %bias; 
our $MINIMUM_READS = 0; 
sub new { 
   my $class = shift; 
   my $self = {}; 
   $self->{'total_read_count'}=0;
   $self->{'total_watson_count'}=0; 
   $self->{'total_crick_count'} = 0; 
   $self->{'xmers'} = []; 
   bless $self, $class; 
   if (@_){ 
      $self->parseReport(@_); 
   }
   return $self; 
}


sub parseReport { 
   my $self = shift; 
   my $file = shift; 
   my $fh = gensym(); 
   open($fh, $file) or warn "$PKG:$!:bad open file : $file\n"; 
   my $xmer = undef; 
   my $count = undef; 
   while(my $line = <$fh>){
      chomp $line ; 
      if ($line=~/SampleTotalReadCount/){
            ($xmer, $count) = split "\t", $line; 
            $self->{'SampleTotalReadCount'} = $count;
      }elsif ($line=~/^[ATGC]+/){
         #we are starting an xmer; 
         ($xmer, $count) = split "\t",$line; 
	 next unless $count >= $MINIMUM_READS;
	 $self->{'total_read_count'} += $count;  
	 $self->{$xmer}->{'count'} = $count; 
         push @{$self->{xmers}}, $xmer; 
      }else { 
	 #we are adding reads to the existing xmer
         my ($blank, $sequence, $count, @rest) = split "\t",$line; 
         while (@rest >0) { 
	    warn "shifting because of weirdness\n"; 
	    $sequence = $count; 
	    $count    = shift @rest; 
         }	
         warn "seq:$sequence\ncount:$count\n"; 
         my $strand = undef; 
         if ($count=~/\*/){ 
            $strand='crick'; 
            $count = $1 if $count=~/(\d+)/;
	    $self->{'total_crick_count'} += $count;
	    $self->{$xmer}->{'crick_count'} += $count; 
            $sequence = revcomp($sequence); 
         }else { $strand ='watson';
		 $self->{'total_watson_count'} += $count; 
		 $self->{$xmer}->{'watson_count'} += $count;
         }  
         $self->{$xmer}->{$sequence}->{$strand} = $count;
         $self->{$xmer}->{$sequence}->{'count'} += $count;
      }
   }
}
sub total_read_count { 	
   my $self = shift; 
   return $self->{'total_read_count'};
}
sub sample_total_reads { 
    my $self = shift; 
    return $self->{'SampleTotalReadCount'}; 
} 
sub total_repeat_read_count { 
    my $self = shift; 
    return $self->{'total_read_count'}; 
} 

sub watson_read_count{
   my $self = shift; 
   return $self->{'total_watson_count'}; 
}
sub crick_read_count { 
   my $self = shift; 
   return $self->{'total_crick_count'}; 
}
sub count_for_xmer {
   my $self = shift; 
   my $xmer = shift; 
   my $ret = $self->{"$xmer"}->{'crick_count'} + $self->{"$xmer"}->{'watson_count'};
   return $ret; 
}
sub xmers_by_count_ascending { 
   my $self = shift; 
   my @ret = map { $_->[0] } sort {$a->[1] <=> $b->[1]}map {[$_,$self->{$_}->{'count'}]} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_count_descending { 
   my $self = shift; 
   my @ret = map { $_->[0] } sort {$b->[1] <=> $a->[1]}map {[$_,$self->{$_}->{'count'}]} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_gc_ascending { 
   my $self = shift; 
   my @ret = sort { _gcp($a) <=>_gcp($b) } @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_gc_descending { 
   my $self = shift; 
   my @ret = sort { _gcp($b) <=>_gcp($a) } @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_length_ascending { 
   my $self = shift; 
   my @ret = sort {length($a) <=> length($b)} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_length_descending { 
   my $self = shift; 
   my @ret = sort { length($b) <=> length($a) } @{$self->{'xmers'}}; 
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_watson_ascending { 
   my $self = shift; 
   my @ret = map{$_->[1]}sort{_getWatsonPercentage($a->[0]) <=> _getWatsonPercentage($b->[0])}map{[$self->{$_},$_]}@{$self->{'xmers'}}; 
   return wantarray ? @ret : \@ret; 
}
sub _getWatsonPercentage { 
   my ($watson, $crick) = undef; 
   my $hash = shift; 
   foreach my $seq (keys %{$hash}) { 
      #there is a key 'count', we skip that one and only look at seqs, 
      next if ($seq eq 'count' || $seq !~/^[ATGC]+$/); 
      $watson += $hash->{$seq}->{'watson'}; 
      $crick  += $hash->{$seq}->{'crick'}; 
   }
   return 0 unless $watson; 
   return 100 unless $crick; 
   my $t=$watson+$crick; 
   my $p = 100*($watson/$t);
   return $p; 
}
 
sub _gcp {
   my $s = shift; 
   my $t = length $s; 
   my $gc = $s=~tr/cCgG/cCgG/; 
   my $gcp = 100 * ($gc/$t); 
   return $gcp; 
}
sub set_xmers { 
   my $self = shift; 
   $self->{'xmers'} = shift; 
}

1; #IT TRUE, IT MAGIC, 
