package CDK::RNAiScreens;

use strict;
use warnings;
use Symbol qw/gensym/;
use CDK::Object; 

our @ISA = qw(CDK::Object Exporter);
our $VERSION = '0.01';

our $PKG = 'CDK::RNAiScreens';
sub new { 
    my $that = shift; 
    my $class = ref($that) || $that;
    my $self = $class->SUPER::new(@_); 
    bless $self, $class; 
    if (@_) { 
        $self->ScreenFile(@_); 
        $self->parse(); 
    }
    return $self; 
} 

sub screenFile ($){
    my $self = shift; 
    my $file = shift; 
    $self->ScreenFile($file) if $file;
    return $self->ScreenFile();  
} 

sub parse () { 
    my $self = shift; 
    my $header      = CDK::Object->new(); 
    my $screen      = CDK::Object->new(); 
    my $screenMap   = CDK::Object->new(); 
    my $screenData  = []; 
    my $screenFields= []; 
    my $fh = gensym();  
    open($fh, $self->ScreenFile) or die "$!\n";
    my $reset_state = sub {
        $header         = CDK::Object->new(); 
        $screen         = CDK::Object->new(); 
        $screenData     = []; 
        $screenFields   = []; 
        $screenMap       = CDK::Object->new(); 
    };
    while(<$fh>){
        chomp;
        if (/^#/ and /=/) { #header of screen
            s/#//;
            my($k,$v) = split "=", $_;
            $header->set(ucfirst $k=>$v);
            if ( $k=~tr/ /_/ ) { 
                $header->set(ucfirst $k=>$v);
            }
        }elsif (/^#/ ) { #fields collected for this screen, could be different per screen.
            s/#//;
            $screenFields = [split "\t", $_];
        }elsif (!/^#/i and $_ and ! m@//@) { # non blank line, starting new screen data section
            my @fields = split "\t", $_;
            my $datum  = CDK::Object->new(); 
            for(my $i=0;$i<scalar @{ $screenFields }; $i++){
                $datum->set(ucfirst $screenFields->[$i]=>$fields[$i]);
                if ( $screenFields->[$i]=~tr/ /_/ ) {
                    $datum->set(ucfirst $screenFields->[$i]=>$fields[$i]);
                }
            }

            #push @{ $screenData },  \@fields ;
            push @{ $screenData },  $datum ;
            if ($fields[1] gt "") { 
                #$screenMap->pushOn($fields[1] => \@fields); 
                $screenMap->pushOn($fields[1] => $datum); 
            }if($fields[2] gt ""){
                #$screenMap->pushOn($fields[2] => \@fields); 
                $screenMap->pushOn($fields[2] => $datum); 
            }if($fields[3] gt ""){
                #$screenMap->pushOn($fields[3] => \@fields); 
                $screenMap->pushOn($fields[3] => $datum); 
            } 
        }elsif ($_=~m@^//$@){ #end of screena
            $screen->Headers($header);
            $screen->Fields($screenFields);
            $screen->Data($screenData);
            $screen->Map($screenMap); 
            $self->pushOn("Screens",$screen) if ($screen && ref($screen));
            $reset_state->();
        }
    }
}

# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::RNAiScreens - Perl extension for blah blah blah

=head1 SYNOPSIS

    use CDK::RNAiScreens;
    my $RNAiScreens = CDK::RNAiScreens->new(@ARGV); 
     #or do it explicitly the long way 
     # $RNAiScreens=CDK::RNAiScreens->new();
     #  $RNAiScreens->ScreenFile($file);
     #  $RNAiScreens->parse();
     #

    my @screens = $RNAiScreens->Screens;
    foreach my $screen ($RNAiScreens->Screens){
        my $headers = $screen->Headers;
        #HEADERS ARE TYPICALLY
        #Stable_ID, Screen_Title, Authors, Publication_Year,
        #Pubmed_ID, Organism, Screen_Type, Biosource, Biomodel, Assay, Method,
        #Library_Manufacturer, Library, Scope, Reagent_Type, Score_Type, Cutoff,
        #Notes
        print $screen->Headers->Screen_Title, "\n";
        my $pubDate = $screen->Headers->Publicate_Year; 
           $pubData = $headers->Publication_Year; 
        if ($screen->Headers->Organism eq 'Drosophila Melanogaster') { 
            print "fly";
        }
        print $headers->Screen_Title, "\n";
        my $title = $headers->get("Screen_Title");
        print "title:$title\n";
        my $title = $headers->Screen_Title;
        print "title:$title\n";
        my $title = $screen->Headers->Screen_Title;
        print "title:$title\n";

        my $fields = $screen->Fields;

        #FIELDS typically
        #Stable ID, Entrez ID, Gene ID, Gene Symbol, Reagent ID,Score,Phenotype          #Conditions, Follow Up, Comment
        print $fields->[0];
        my @fields = $screen->Fields;
        print join ":", @fields;
        print "\n"; 
        print join ";", $screen->Fields;
        print "\n"; 
        #DATA IS LIST OF LIST
        my $data = $screen->Data;
        print join "\t", $screen->Fields;
        print "\n";
        foreach my $entry ( @{ $data } ){
            my $gID = $entry->Gene_ID; 
            my $score = $entry->Score; 
            my $phenotype = $entry->Phenotype; 
        }
    }

=head1 DESCRIPTION

CDK::RNAiScreens : A module that makes RNAiScreen data accessible through objects and methods. 
                   A RNAiScreen Object has a LIST of SCREENS. 
                   Each Screen has a HEADER, a list of FIELDS, and DATA
                   See the above section for typical usage. 

=head2 EXPORT

None.



=head1 SEE ALSO

CDK::Object 

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
