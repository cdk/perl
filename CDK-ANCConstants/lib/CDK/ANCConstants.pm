package CDK::ANCConstants;
use strict;
require Exporter; 
our $VERSION = 1.0;
our @ISA = qw(Exporter);
our @EXPORT = qw(); #we don't contaminate your name space with our bizness

our @EXPORT_OK = qw(ACCESSION AMPLIFICATION_FORWARD AMPLIFICATION_REVERSE AMPLIFICATION_SIZE ASSEMBLY_ID CENTER_NAME CENTER_PROJECT CHEMISTRY CHEMISTRY_TYPE CHIP_DESIGN_ID CHROMOSOME CHROMOSOME_REGION CLIP_LEFT CLIP_QUALITY_LEFT CLIP_QUALITY_RIGHT CLIP_RIGHT CLIP_VECTOR_LEFT CLIP_VECTOR_RIGHT CLONE_ID COMPRESS_TYPE CULTIVAR_GROUP CVECTOR_ACCESSION CVECTOR_CODE ENVIRONMENT_TYPE HI_FILTER_SIZE HOST_CONDITION HOST_ID HOST_LOCATION HOST_SPECIES INDIVIDUAL_ID INSERT_SIZE INSERT_STDEV ITERATION LIBRARY_ID LO_FILTER_SIZE PICK_GROUP_ID PLACE_NAME PLATE_ID POPULATION_ID PREP_GROUP_ID PRIMER PRIMER_CODE PROGRAM_ID REFERENCE_ACCESSION REFERENCE_OFFSET RUN_DATE RUN_GROUP_ID RUN_LANE RUN_MACHINE_ID RUN_MACHINE_TYPE SEQ_LIB_ID SOURCE_TYPE SPECIES_CODE STRATEGY SUBMISSION_TYPE SUBSPECIES_ID SVECTOR_ACCESSION SVECTOR_CODE TEMPLATE_ID TI TRACE_DIRECTION TRACE_END TRACE_FORMAT TRACE_NAME TRACE_TYPE_CODE TRANSPOSON_ACC TRANSPOSON_CODE WELL_ID);

our %EXPORT_TAGS = (all=>\@EXPORT_OK);

sub ConstantList {
   return @EXPORT_OK; 
}

sub ACCESSION{return 0};
sub AMPLIFICATION_FORWARD{return 1};
sub AMPLIFICATION_REVERSE{return 2};
sub AMPLIFICATION_SIZE{return 3};
sub ASSEMBLY_ID{return 4};
sub CENTER_NAME{return 5};
sub CENTER_PROJECT{return 6};
sub CHEMISTRY{return 7};
sub CHEMISTRY_TYPE{return 8};
sub CHIP_DESIGN_ID{return 9};
sub CHROMOSOME{return 10};
sub CHROMOSOME_REGION{return 11};
sub CLIP_LEFT{return 12};
sub CLIP_QUALITY_LEFT{return 13};
sub CLIP_QUALITY_RIGHT{return 14};
sub CLIP_RIGHT{return 15};
sub CLIP_VECTOR_LEFT{return 16};
sub CLIP_VECTOR_RIGHT{return 17};
sub CLONE_ID{return 18};
sub COMPRESS_TYPE{return 19};
sub CULTIVAR_GROUP{return 20};
sub CVECTOR_ACCESSION{return 21};
sub CVECTOR_CODE{return 22};
sub ENVIRONMENT_TYPE{return 23};
sub HI_FILTER_SIZE{return 24};
sub HOST_CONDITION{return 25};
sub HOST_ID{return 26};
sub HOST_LOCATION{return 27};
sub HOST_SPECIES{return 28};
sub INDIVIDUAL_ID{return 29};
sub INSERT_SIZE{return 30};
sub INSERT_STDEV{return 31};
sub ITERATION{return 32};
sub LIBRARY_ID{return 33};
sub LO_FILTER_SIZE{return 34};
sub PICK_GROUP_ID{return 35};
sub PLACE_NAME{return 36};
sub PLATE_ID{return 37};
sub POPULATION_ID{return 38};
sub PREP_GROUP_ID{return 39};
sub PRIMER{return 40};
sub PRIMER_CODE{return 41};
sub PROGRAM_ID{return 42};
sub REFERENCE_ACCESSION{return 43};
sub REFERENCE_OFFSET{return 44};
sub RUN_DATE{return 45};
sub RUN_GROUP_ID{return 46};
sub RUN_LANE{return 47};
sub RUN_MACHINE_ID{return 48};
sub RUN_MACHINE_TYPE{return 49};
sub SEQ_LIB_ID{return 50};
sub SOURCE_TYPE{return 51};
sub SPECIES_CODE{return 52};
sub STRATEGY{return 53};
sub SUBMISSION_TYPE{return 54};
sub SUBSPECIES_ID{return 55};
sub SVECTOR_ACCESSION{return 56};
sub SVECTOR_CODE{return 57};
sub TEMPLATE_ID{return 58};
sub TI{return 59};
sub TRACE_DIRECTION{return 60};
sub TRACE_END{return 61};
sub TRACE_FORMAT{return 62};
sub TRACE_NAME{return 63};
sub TRACE_TYPE_CODE{return 64};
sub TRANSPOSON_ACC{return 65};
sub TRANSPOSON_CODE{return 66};
sub WELL_ID{return 67};

#pod documentation follows

=head1 NAME
 CDK::ANCConstants
 
=head1 SYNOPSIS

 use strict;
 use CDK::ANCConstants qw/:all/;
 use CDK::ANCFileReader; 
 #...
 my $reader = CDK::ANCFileReader->new($fh);
 #examine what constants are available
 print join "\n", CDK::ANCConstants::ConstantList(); 
 #print some data
 while( my $data = $reader->getNextEntry() ){
    print $data->[TI], "\n" ;
    print $data->[TEMPLATE_ID], "\n";	
 }

=head1 Copyright

 Cameron D Kennedy, 2007
 
=cut
 
1; # IT TRUE 
