package CDK::ListFilter ; 
use strict;
use CDK::Object; 
our @ISA = qw/CDK::Object/; 

our $DEBUG = 1;
our $PKG = 'CDK::ListFilter';

sub new {
   my $that = shift;
   my $class = ref($that) ||$that;
   my CDK::ListFilter $self = CDK::Object->new(); 
   bless $self, $class unless ref($self) eq $class;
   my $filter = sub {return 1;};
   $self->Filter($filter);
   return $self;
}
=head1 CDK::ListFilter

 a module for filtering a list, via a user defined function

=cut

=head1 FUNCTIONS

    these are the functions this module exposes to the end user

=cut 

=item setFilter

    The setFilter function lets you, the end user, set the filtering function for the CDK::ListFilter Object. 

=cut



sub setFilter {
   my $self = shift; 
   my $filter = shift;
   if ($DEBUG) {
	warn "$PKG : bad filter" unless ref($filter) eq 'CODE';
   }
    $self->Filter($filter); 
}

=item getFilter 

    getFilter returns the filter object.  You probably won't ever need this, except perchance for debugging purposes. 

=cut

sub getFilter {
   my $self = shift;
   return $self->Filter; 
}

=item getValid

    getValid is a function that returns items in the list that are valid according the the user defined filter function

=cut


sub getValid {
   my $self = shift; 
   #my @files = @_;  expensive to copy for many files
   my @valid =(); 
   my $filter = $self->Filter;
   @valid = grep { $filter->($_) ? 1 : 0 } @_; 
   return wantarray ? @valid : \@valid;
}

=item getInvalid 

    getInvalid does the opposite of getValid,  returning the items that fail the filter test

=cut

sub getInvalid {
   my $self = shift; 
   my @invalid = "";
   my $filter = $self->Filter;
   @invalid = grep { $filter->($_) ? 0 :1 }@_; 
   return wantarray ? @invalid : \@invalid;
}

=end

1; #IT MAGIC, IT TRUE. 

