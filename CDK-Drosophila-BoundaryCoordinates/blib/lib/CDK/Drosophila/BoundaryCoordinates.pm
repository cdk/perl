package CDK::Drosophila::BoundaryCoordinates;

use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use CDK::Drosophila::BoundaryCoordinates ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( 
                                    coordinateMap
                                    S2coordinateMap
                                    getS2Coordinates
                                    getFlyCoordinates
	                              )
                               ]
                    ); 

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';

our $data = {   'X'     =>  22030326,
                '2L'    =>  22000975,
                '2R'    =>  1285689,
                '3L'    =>  22955576,
                '3R'    =>  378656  } ; 

our $s2_data = {    'X'     =>  21240000,
                    '2L'    =>  21090000,
                    '2R'    =>  2500000,
                    '3L'    =>  22250000,
                    '3R'    =>  1    }; 

sub coordinateMap { 
    return wantarray ? %{ $data } : $data; 
} 
sub S2coordinateMap () {
    return wantarray ? %{ $s2_data } : $s2_data; 
} 
sub getFlyCoordinates () { 
    return wantarray ? %{ $data } : $data; 
} 
sub getS2Coordinates () {
    return wantarray ? %{ $s2_data } : $s2_data; 
} 



1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Drosophila::BoundaryCoordinates - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Drosophila::BoundaryCoordinates;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::Drosophila::BoundaryCoordinates, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO


X
AE014298 : 22030326..22422827
XHet
CM000460 : 1..204112



2L
AE014134 : 22000975..23011544
2LHet
CM000456 : 1..368872



2R
AE013599 : 1..1285689
2RHet
CM000457 : 1..3288761



3L
AE014296 : 22955576..24543557
3LHet
CM000458 : 1..2555491



3R
AE014297 : 1..378656
3RHet
CM000459 : 1..2517507



4
AE014135 : undefined
-
none



Y
none
Y
CM000461 : 1..347038



U*
none
U*
FA000001 : 1..1004903






Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
