package CDK::SPP;

use strict;
use warnings;
use Symbol qw/ gensym /; 
use CDK::SPP::Args; 
require Exporter;
our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw( 
					run_spp 
				        run_spp_bowtie
					set_spp_options
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION 	= '0.01';
our $PKG	= 'CDK::SPP'; 
our $OPTIONS = CDK::SPP::Args->new(); 
sub set_spp_options { 
   if ( ref( $_[0] ) eq 'CDK::SPP::Args'){
      	warn "$PKG: setting OPTIONS to ARG\n"; 
	$OPTIONS = shift; 
   }elsif ( ref ( $_[0] ) eq 'HASH' ) {
	my $href = shift; 
        foreach my $key (keys %{$href} ) { 
	   $OPTIONS->{$key} = $href->{$key};
	}
	
   }elsif (@_) { 
	my %arg = @_; 
   	set_spp_options(\%arg);
   }else{ 
      warn "$PKG: not sure what happened in set_spp_options, didn't get a reference I could handle\n"; 
   }
}

sub run_spp { 
   my $srange =  $OPTIONS->{'binding_characteristics_srange'} ;
   warn "$PKG: srange : $srange->[0], $srange->[1]\n";   
   my $R = gensym(); 
   open (R, "|R --no-save") or die "$PKG: can't open pipe to R \n";
   print R <<"END"
library(spp);
chip.data <- read.eland.tags(\"$OPTIONS->{'experiment'}\", extended=T);
input.data <- read.eland.tags(\"$OPTIONS->{'input'}\", extended=T); 
binding.characteristics <-get.binding.characteristics(chip.data,srange=c($srange->[0],$srange->[1]),bin=$OPTIONS->{'binding_characteristics_bin'},cluster=$OPTIONS->{'cluster'});
chip.data <- select.informative.tags(chip.data,binding.characteristics);
input.data <- select.informative.tags(input.data,binding.characteristics);
chip.data <- remove.local.tag.anomalies(chip.data);
input.data <- remove.local.tag.anomalies(input.data);
smoothed.density <- get.smoothed.tag.density(chip.data,control.tags=input.data,bandwidth=$OPTIONS->{'smoothed_tag_density_bandwidth'},step=$OPTIONS->{'smoothed_tag_density_step'},tag.shift=round(binding.characteristics\$peak\$x/2)); 
writewig(smoothed.density,\"$OPTIONS->{'name'}.density.wig\",\"$OPTIONS->{'name'} smoothed, background-subtracted tag density\");
rm(smoothed.density);
enrichment.estimates <- get.conservative.fold.enrichment.profile(chip.data,input.data,fws=2*binding.characteristics\$whs,step=$OPTIONS->{'conservative_enrichment_profile_step'},alpha=$OPTIONS->{'conservative_enrichment_profile_alpha'});
writewig(enrichment.estimates,\"$OPTIONS->{'name'}.enrichment.estimates.wig\",\"$OPTIONS->{'name'} conservative fold-enrichment/depletion estimates shown on log2 scale\");
rm(enrichment.estimates);
q();
y;
END

}

sub run_spp_bowtie { 
   my $R = gensym(); 
   open (R, "|R --no-save") or die "$PKG: can't open pipe to R \n";
   print R <<"END"
library(spp);
chip.data <- read.bowtie.tags(\"$OPTIONS->{'experiment'}\");
input.data <- read.bowtie.tags(\"$OPTIONS->{'input'}\"); 
binding.characteristics <-get.binding.characteristics(chip.data,srange=c($OPTIONS->{'binding_characteristics_srange'}->[0],$OPTIONS->{'binding_characteristics_srange'}->[1]),bin=$OPTIONS->{'binding_characteristics_bin'},cluster=$OPTIONS->{'cluster'});
chip.data <- select.informative.tags(chip.data,binding.characteristics);
input.data <- select.informative.tags(input.data,binding.characteristics);
#//chip.data <- remove.local.tag.anomalies(chip.data);
#//input.data <- remove.local.tag.anomalies(input.data);
smoothed.density <- get.smoothed.tag.density(chip.data,control.tags=input.data,bandwidth=$OPTIONS->{'smoothed_tag_density_bandwidth'},step=$OPTIONS->{'smoothed_tag_density_step'},tag.shift=round(binding.characteristics\$peak\$x/2)); 
writewig(smoothed.density,\"$OPTIONS->{'name'}.density.wig\",\"$OPTIONS->{'name'} smoothed, background-subtracted tag density\");
rm(smoothed.density);
enrichment.estimates <- get.conservative.fold.enrichment.profile(chip.data,input.data,fws=2*binding.characteristics\$whs,step=$OPTIONS->{'conservative_enrichment_profile_step'},alpha=$OPTIONS->{'conservative_enrichment_profile_alpha'});
writewig(enrichment.estimates,\"$OPTIONS->{'name'}.enrichment.estimates.wig\",\"$OPTIONS->{'name'} conservative fold-enrichment/depletion estimates shown on log2 scale\");
rm(enrichment.estimates);
q();
y;
END

}

	
# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::SPP - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::SPP qw/ set_spp_options run_spp /;
  set_spp_options($CDK::SPP::Args)
  run_spp(); 


=head1 DESCRIPTION

set the options, run the spp package. 

=head2 EXPORT

None by default.
set_spp_options
run_spp


=head1 SEE ALSO

CDK::SPP::Args

=head1 AUTHOR

cameron kennedy, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
