package CDK::FileFinder;

use strict;
use CDK::Iterator;
use CDK::List; 
use CDK::Object; 
use CDK::ListFilter; 

use subs qw/DEBUG/; 

our @ISA = qw/CDK::ListFilter CDK::Object/;
$CDK::FileFinder::DEBUG = 1;
$CDK::FileFinder::RECURSE = '1';
our $PKG = 'CDK::FileFinder';

=head1 CDK::FileFinder

    A module to find files using the a user defined filter 

=cut

=head1 CLASS METHODS 

=item setRecurse 

    CDK::FileFinder->setRecurse(1): #activate the recursiveness

=cut

sub setRecurse { $CDK::FileFinder::RECURSE = shift @_ }

sub DEBUG { warn join " ",$PKG,@_ if $CDK::FileFinder::DEBUG}

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my $self = $class->SUPER::new(); 
   return bless $self, $class;
}

#sub new {
#   my $that = shift;
#   my $class = ref($that) || $that;
#   my $self = $class->SUPER::new(); 
#   warn "self is object" if $self->isa('CDK::Object'); 
#   warn "self is ListFilter" if $self->isa('CDK::ListFilter'); 
#   warn "self = $self, $self->class(), ref($self)";
#   $self->SearchDirectories([]); 
#   $self->Files([]); 
#   $self->Valids([]); 
#   $self->Recurse(1); #default is to recurse;    
#   return $self;
#}

=head1 Instance Methods (Object Methods) 

=item setSearchDirectory() 
    
    This well named function sets the directories in which the searching for files will take place. 

=cut


sub setSearchDirectory {
   my $self = shift;
   #we handles arrays and array refs. 
   #expand refs to Lists, and get rid of trailing slashes. 
   my @dirs =map{s@/$@@; $_} map { ref($_) ? @{ $_ } : $_ } @_;
   $self->pushOn('SearchDirectories', @dirs); 
}

=item getSearchDirectoryArrayRef

    getSearchDirectoryArrayRef is a function that returns an array of the directories that are to be searched.  Mostly used for debugging purposes. 

=cut

sub getSearchDirectoryArrayRef { 
   my $self = shift; 
   #really, just call $obj->SearchDirectories in scalar context to get array reference instead of copy of array 
   my $dir  = $self->SearchDirectories; 
   return $dir; 
}
sub getSearchDirectoryList {
   my $self = shift; 
   return CDK::List->new($self->SearchDirectories());
}
sub getSearchDirectoryString {
   my $self = shift; 
   return join ",", $self->SearchDirectories; 
}
sub getSearchDirectoryIterator {  
   my $self = shift;
   return CDK::Iterator->new( $self->SearchDirectories ); 
}

=item setSearchFileFilter($filter)

    The setSearchFileFilter function takes an anonymous subroutine as the argument, and uses the return value from the subroutine to determine if an item being tested is valid or not.   

=cut

sub setSearchFileFilter {
   my $self = shift;
   my $sub = shift;
   $self->setFilter($sub); 
}

=item search 

    You guessed it, call the search function on the CDK::FileFinder object to start the searching

=cut

sub search {
   my $self = shift;
      my $itr = $self->getSearchDirectoryIterator();
      my $dir = undef;
      while ( $itr->hasNext() ){
         $dir = $itr->next();
         DEBUG("processing $dir in search"); 
         $self->_process_dir( $dir ); 
      }
   _update_valid_list($self);
   
}

=item getValidFiles 

    the getValidFiles subroutine is the way to get back the files that passed the filter test

=cut

sub getValidFiles {
   my $self = shift;
   my @ret = $self->Valids(); 
   return wantarray ? @ret : \@ret; 
}

=item getInvalidFiles 
   
the getInvalidFiles function is the inverse of the getValidFiles function.  It returns files that don't pass the filter test

=cut   

sub getInvalidFiles { 
  my $self = shift; 
  my @ret = undef; 
  if (wantarray) { 
    @ret = $self->Invalids();
  }
}

sub _process_dir {
   my $self = shift; 
   my $dir = shift;
   my @files = undef;
   die unless $dir; 
   warn "$PKG : _processing_dir $dir\n" if $CDK::FileFinder::DEBUG;

    
   foreach my $f ( @{ [ glob ("$dir/*") ]} ){
      $self->_process_dir($f) if ( (-d $f) and (($CDK::FileFinder::RECURSE > 0) or ($self->{'RECURSE'} > 0)) );
      $self->pushOn('Files', $f) if -f $f ; 
   }
}
sub _update_valid_list {
   my $self = shift; 
   my $filter = $self->Filter(); 
   warn "$PKG : filtering with filter: $filter\n" if $CDK::FileFinder::DEBUG;
   my @files = $self->Files(); 
   my @valid = $self->getValid(@files); 
   my @invalid = $self->getInvalid(@files); 
   $self->set('Invalids', []); 
   $self->pushOn('Invalids', ( $self->getInvalid(@files))); 
   $self->pushOn('Valids'  , ( $self->getValid(@files) ));
}

=item setRecurse 

    By default we recursively traverse directories.  Sometimes these settings change at the class level.  This function changes the recurse properties at the object level. 

=cut


sub setRecurse {
	my $self = shift;
	my $r = shift @_;
 	warn "$PKG : setting RECURSE = $r\n" if $CDK::FileFinder::DEBUG;
   $self->Recurse($r); 
}

=item reset 

    return the object to its prestine state of newness

=cut 

sub reset {
	my $self = shift;
	return CDK::FileFilter->new(); 
}


1; # it true , IT MAGIC, WOOOOOO YEAH 
