package CDK::Mascot::PeptideHit;

use strict;
use warnings;
use CDK::Object; 
our @ISA = qw(CDK::Object);

our $VERSION = '0.01';
our $PKG     = 'CDK::Mascot::PeptideHit'; 
our $DEBUG = 0; 
sub DEBUG_ON { $DEBUG=1}
sub DEBUG_OFF {$DEBUG=undef}
sub DEBUG { warn "$PKG: @_\n" if $DEBUG} 

sub searchable_properties { 
   my $self = shift; 
   my @ret=  grep !/ProteinHit/,$self->properties(); 
   return wantarray ? @ret : \@ret; 
} 

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::PeptideHit - Perl module to store information about a peptide hit within a protein hit

=head1 SYNOPSIS

  use CDK::Mascot::PeptideHit;
  my $peptideHit = CDK::Mascot::PeptideHit->new(); 
  $peptideHit->set('Score', 34); 
  $peptideHit->Score(34); 
  $peptideHit->set('Mods', 'Methyl'); 
  $peptideHit->set('Mods', 'Phospho'); 
  @mods = $peptideHit->Mods; 
  
=head1 DESCRIPTION

Typically this is used in CDK::Mascot::Protein Objects created in CDK::MascotCSV::File Objects.
Access properties with either $pepHit->get('Property') or $pepHit->Property

 $score = $pepHit->Score; 
 $Seq   = $pepHit->get('Seq'); 

=head1	PROPERTIES

	Query
	Rank
	Isbold
	Exp_mz
	Exp_mr
	Exp_z
	Calc_mr
	Delta
	Miss
	Score
	Homol
	Ident
	Expect
	Res_before
	Seq
	Res_after
	Var_mod
	Var_mod_pos
	


=head2 EXPORT

None by default.



=head1 SEE ALSO

CDK::ProteinHit CDK::Object CDK::MascotCSV::File 

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
