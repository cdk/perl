package CDK::ClipFileReader;
use strict;
our $VERSION = 1.0;
our $PKG = 'CDK::ClipFileReader';
sub new { 
   my $class = shift; 
   my $self = {}; 
   bless $self, ref($class)||$class;
   my $fh = shift;
   if ($fh){
      $self->setFileHandle($fh);
   }
   return  $self; 
}
sub setFileHandle{
   my $self = shift;
   $self->{'-filehandle'} = shift;
}
sub getFileHandle{
	my $self = shift;
	return $self->{'-filehandle'}; 
}
sub getNextEntry {
   my $self = shift;
   my $fh = $self->getFileHandle();
   return undef unless my $line = <$fh>;
   $line=~/^(\d+)\s+(\d+)\s+(\d+)/;
   $self->getNextEntry() unless $1;
   return bless {'-trace_id'=>"$1", 
	   '-start'   =>"$2",
	   '-end'     =>"$3" }, "ClipFileEntry" ;
}
1; #it true
{
package ClipFileEntry; 
sub getTraceID {return $_[0]->{'-trace_id'}}
sub getStart {return $_[0]->{'-start'}}
sub getEnd {return $_[0]->{'-end'}}
} #end package ClipFileEntry

1; #it still true 

=pod

=head1 NAME 

 CDK::ClipFileReader

=head1 DESCRIPTION

 A perl module to read in data from Clip File from NCBI

=head1 SYNOPSIS

 use CDK::ClipFileReader; 
 use Symbol qw/gensym/;
 my $fh = gensym; 
 my $file = shift @ARGV; 
 open ($fh, $file) or die "bang $!";
 my $reader = CDK::ClipFileReader->new($fh);
 while(my $entry = $reader->getNextEntry()){
    # do somethign with entry 
    #it is a hash reference
    print $entry->getTraceID(); 
    my($start, $end) = ($entry->getStart, $entry->getEnd); 
 }

=head1 METHODS 

 setFileHandle($fh) 
 sets the file handle inside the reader to $fh
 $reader->setFileHandle($fh); 

 getFileHandle() 
 get the current file handle being used in the reader
 my $fh = $reader->getFileHandle(); 

 getNextEntry()
 get the next entry in the file
 my $entry = $reader->getNextEntry(); 

=head1 Copyright

 Cameron D Kennedy, 2007
 All rights reserved, no gaurantee, no warranty

=cut

1; # still true 
