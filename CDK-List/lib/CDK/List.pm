package CDK::List;

use strict;
use CDK::Iterator;
use CDK::Object; 

our @ISA = qw/CDK::Object/; 
sub new {
	my $that = shift;
	my $class = ref($that) || $that;
	#my $self = [];
    #    if (scalar @_ == 1 && ref($_[0]) eq 'ARRAY'){ 
	#   #REPLACE SELF WIT ARRAY ARGUMENT
	#   $self=shift @_; 
    #    }
    my $self = $class->SUPER::new(); 
    $self->List([@_]); 
	bless $self, $class;
	return $self;
}
sub add {
	my $self = shift;
    $self->pushOn('List', @_); 
	#push @{$self}, @_;
}
sub count {
	my $self = shift;
	#return scalar @{ $self } ;
    my $list = $self->List; 
	return scalar @{ $list } ;

}
sub getIterator {
	my $self = shift;
    my $list = $self->List; 
	return new CDK::Iterator (@{ $list });
}
sub getArray {
	my $self = shift;
    my $list = $self->List; 
	return $list ? @{ $list } : undef;
}

1; #it true
__END__

#DOCUMENTATION FOLLOWS
=head1 NAME

CDK::List   A perl extension for managing a list 

=head1 SYNOPSIS

  use CDK::List;
  my $list = CDK::List->new(); 
  $list->add("dog"); 
  $list->add("cat"); 
  @array = $list->getArray(); 
  $itr   = $list->getIterator(); 

=head1 DESCRIPTION

CDK::List keeps a list in an array, and provides methods to add to a list. 
There is no way to remove an item, make a new list for that. 
Access is provided to both the array, and an  Iterator for the array. 

=head1 EXPORT

None by default.



=head1 SEE ALSO

CDK::Iterator
CDK::ChromosomeList

=head1 AUTHOR

cameron kennedy , E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut




