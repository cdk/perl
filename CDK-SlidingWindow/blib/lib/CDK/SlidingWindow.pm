package CDK::SlidingWindow;

our $VERSION = '0.01';

sub new { 
   my $class = shift; 
   $class=ref($class)||$class; 
   my $self ={'-window'=>[], '-inFH'=>'', '-outFH'=>'', -windowSize=>'', '-sum'=>''}; 
   return bless $self, $class ; 
}
sub inFH { 
   my $self = shift;
   $self->{'-inFH'} = shift if @_; 
   return $self->{'-inFH'}; 
}  
sub outFH { 
   my $self = shift; 
   $self->{'-outFH'} = shift if @_ ; 
   return $self->{'-outFH'} 
} 
sub windowSize{ 
   my $self = shift; 
   $self->{ '-windowSize'} = shift if @_ ; 
   return $self->{'-windowSize'} ; 
}
sub GO_GO_WINDOWER { 
   my $self = shift; 
   my ($arm, $position, $count); 
   my @window = @{  $self->{'-window'} } ; 
   #print "windows is $window\n"; 
   my $in = $self->inFH(); 
while(<$in>){   
      chomp;    
      #put the data into the windowa
      my @data = split("\t",$_); 
      push @window,\@data; 
      $self->{'-sum'} += $data[2]; 
      #blatant forcing of @window into scalar context for comparison . . . 
      if ( @window == $self->windowSize() ) { 
         #buffer is filled and we can print 
         $self->printWindow(\@window);
         $self->{'-last'} = shift @window; 
         $self->{'-sum'} -=$self->{'-last'}->[2] ;  
      }
}
}
sub size { 
   my $self = shift; 
   return $self->{'-size'} ; 
} 
sub printWindow { 
   my $self =shift; 
   my $i = int( ( $self->windowSize() ) / 2 ); 
   my $window = shift; 
   my $outFH = $self->outFH(); 
   my $point = $window->[$i]; 
   print $outFH join "\t", ( $window->[$i]->[0], $window->[$i]->[1]) ;  
#   my $sum ;
#   foreach ( @$window ) {
#      print @{ $_ } ;  
#      $sum+=$_->[2]; 
#   } 
   print $outFH "\t",$self->{'-sum'} , "\n"; 
}

    
         
   


1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::SlidingWindow - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::SlidingWindow;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::SlidingWindow, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.


=head1 HISTORY

=over 8

=item 0.01

Original version; created by h2xs 1.23 with options

  -XC
	CDK::SlidingWindow

=back



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron kennedy, E<lt>cameron@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
