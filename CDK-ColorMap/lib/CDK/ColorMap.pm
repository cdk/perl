package CDK::ColorMap;

use strict;
use warnings;

require Exporter;
our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw(red blue grey rainbow) ] ); 
our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(); 
our $VERSION = '0.01';

my @map = ("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F") ;
sub i2hx { my $i =shift; my $a = $map[int($i/16)]; my $b=$map[($i%16)]; my $r=$a.$b; return $r}
sub red {
   my @return; 
   for (my $i=255;$i>=0;$i--){
      my $hxa = $map [ (int $i/16) ] ; 
      my $hxb = $map [ ($i%16) ] ;
      my $j = $hxa . $hxb;
      my $rgb = "#FF${j}${j}";
      push @return, $rgb;
	}
	return wantarray ? @return : \@return ; 
}

sub blue { 
   my @return; 
   for (my $i=255;$i>=0;$i--){
      my $hxa = $map [ (int $i/16) ] ; 
      my $hxb = $map [ ($i%16) ] ;
      my $j = $hxa . $hxb;
      my $rgb = "#${j}${j}FF";
      push @return, $rgb;
	}
	return wantarray ? @return : \@return ;
}
sub grey { 
   my @return;
   for (my $i=255;$i>=0;$i--){
      my $hxa = $map [ (int $i/16) ] ;
      my $hxb = $map [ ($i%16) ] ;
      my $j = i2hx($i); #$hxa . $hxb;
      my $rgb = "#${j}${j}${j}";
      push @return, $rgb;
        }
        return wantarray ? @return : \@return ;
}

sub rainbow1 { 
   my @return; 
   for(my $i=50;$i>=0;$i--){
      #my $hxa=$map[ (int $i/16) ];
      #my $hxb=$map[ ( $i%16) ];
      my $j = i2hx($i); #$hxa . $hxb; 
      my $rgb = "#FF00${j}"; 
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return; 
}
sub rainbow2 { 
   my @return; 
   for(my $i=0; $i<=255; $i++){ 
     my $j = i2hx($i); 
     my $rgb = "#FF${j}00";
     push @return, $rgb; 
   }
   return wantarray ? @return : \@return ; 
}
sub rainbow3 { 
   my @return; 
   for(my $i=255;$i>=0;$i--){
      my $j = i2hx($i); 
      my $rgb = "#${j}FF00"; 
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return; 
}
sub rainbow4 {
   my @return; 
   for(my $i=0; $i<=255; $i++){ 
      my $j = i2hx($i); 
      my $rgb = "#00FF${j}"; 
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return ; 
}
sub rainbow5 {
   my @return ; 
   for(my $i=255; $i>=0; $i--){
      my $j = i2hx($i); 
      my $rgb = "#00${j}FF";
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return ; 
}
sub rainbow6 { 
   my @return; 
   for (my $i=0; $i<=255; $i++){
      my $j = i2hx($i); 
      my $rgb = "#${j}00FF";
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return; 
}
sub rainbow7 { 
   my @return; 
   for(my $i=255;$i>=51;$i--){
      my $j = i2hx($i); 
      my $rgb = "#FF00${j}"; 
      push @return, $rgb; 
   }
   return wantarray ? @return : \@return; 
}

sub rainbow { 
   my @return; 
   { no strict qw/refs/; 
   for(my $i=1;$i<=6;$i++){
      my $rainbow = "rainbow".$i; 
      my @rainbow = &{$rainbow}; 
      shift @rainbow if $i >1; 
      push @return, @rainbow; 
   }
   }
   return wantarray ?  @return : \@return; 
}


1; # IT MAGIC, IT TRUE< IT ONE ! 

=head1 NAME

CDK::ColorMap - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::ColorMap;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::ColorMap, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron kennedy, E<lt>cameron@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
