package CDK::BlastRunner;
use strict;
#use Boulder::Blast;
use CDK::BlastParser; 
use File::Temp qw( tempfile tempdir );

our $blast   = '/usr/local/ncbi/blast/bin/blastn'; 
#our $blast   = '/usr/bin/blastall';
#our $PROGRAM = 'blastn';
our $PKG = 'CDK::BlastRunner';
our $F   = 'T'; #default for blast filtering of low complexity
our $THREADS = 2; 
our $tmp_dir = "/tmp/blast_tmp/"; 
sub new { 
   my $that = shift; 
   my $class = ref($that) || $that;
   my $self = { '-DataBase'=>"",
		'-QuerySeq'=>"",
		'-QueryFile'=>"",
		'-BlastData'=>""
	       };
   bless $self, $class;
   return $self;
}

sub setDataBase {
   my $self = shift; 
   my $db = shift;
   $self->{'-DataBase'} = $db;
}

sub getDataBase {
   my $self = shift;
   return $self->{'-DataBase'};
}

sub setQuerySeq {
   my $self = shift; 
   my $seq = shift; 
   $self->{'-QuerySeq'} = $seq;
   $self->_CreateSeqFile();
}
sub getQuerySeq {
   my $self = shift;
   return $self->{'-QuerySeq'};
}
sub setQueryFile {
	my $self = shift;
	my $file = shift;
	$self->{'-QueryFile'} = $file;
}
sub getQueryFile {
   my $self = shift;
   return $self->{'-QueryFile'};
}
sub deleteQueryFile { 
   my $self = shift; 
   unlink($self->{'-QueryFile'}) if -e $self->{'-QueryFile'}; 
}
sub _CreateSeqFile {
   my $self = shift; 
   my $fh;
   my $queryfile;
   (undef, $queryfile) = tempfile("OPEN"=>0, "DIR"=>$tmp_dir);
   #$self->{'-QueryFH'} = undef;  //  uneeded unused 
   #$self->{'-QueryFile'} = $queryfile;
   $self->setQueryFile($queryfile);
   unless ($self->{'-QuerySeq'} =~ /^>/ ){
      $self->setQuerySeq( ">QuerySequence\n" . $self->getQuerySeq() . "\n" );
   }
   open (O, ">$queryfile") or die "wtf ? $queryfile :$!";
   print O $self->getQuerySeq();
}

sub runBlast {
   my $self = shift; 
   my $db = $self->getDataBase();
   my $qf = $self->getQueryFile();
   my $out;
   (undef, $out) = tempfile("OPEN"=>0, "DIR"=>$tmp_dir);
   my $sys_call = join " ", ($blast,"-db ", $db, "-query ", $qf, "-out ", $out, "-num_threads", $THREADS); 
    
   warn "$PKG: $sys_call\n";
   my $return = system($sys_call);
   $return ==0 or warn "$PKG : bad blast system call\n$sys_call";
   return undef if $return != 0; 
   warn "$PKG:where is $out\n"  unless -e $out; 
   #my $parser = Boulder::Blast->new(\*BLAST) or warn "$!:$& : $?\n";
   #my $blast_report = $parser->get();
   my $parser = CDK::BlastParser->new(); 
   $parser->file($out); 
   my $report = $parser->parse(); 
   $self->{'-BlastDataFile'} = $out; 
   $self->{'-BlastData'} = $report; 
   $self->deleteQueryFile(); 
}

sub getBlastData {
   my $self = shift;
   my $ret =  $self->{'-BlastData'}; 
   ###Boulder::Blast->new($self->{'-BlastDataFile'})->get() or die "$PKG: $!:$?:$@\n";
   unlink $self->{'-BlastDataFile'}; 
   return $ret; 
}
sub flushData {
   my $self = shift;
   my $db = $self->getDataBase();
   $self = CDK::BlastRunner->new();
   $self->setDataBase($db);
   #my @delete = ('-BlastData', '-QueryFH', '-QueryFile', '-QuerySeq');
   #foreach my $key (@delete){   
   #   $self->{"$key"} = '';
   #}
   return $self;
}

1;

