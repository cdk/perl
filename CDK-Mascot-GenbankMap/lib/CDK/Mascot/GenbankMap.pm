package CDK::Mascot::GenbankMap;

use strict;
use warnings;
use CDK::Object; 
our @ISA = qw(CDK::Object); 
our $PKG = 'CDK::Mascot::GenbankMap'; 
our $VERSION = '0.01';
our $DEBUG = 0; 
sub DEBUG 	{ warn "$PKG: @_\n" if $DEBUG } 
sub DEBUG_ON 	{ $DEBUG = 1}
sub DEBUG_OFF 	{ $DEBUG = 0}


sub genbank_id_list { 
	my $self = shift; 
	return $self->propertiesList(); 
}

sub has_genbank_id { 
    no warnings qw/uninitialized/; 
	my $self = shift; 
	my $want = shift; 
	my $ids = $self->genbank_id_list(); 
	my $ret = grep {$_ eq $want } $ids->getArray(); 
	return $ret; 
}
 

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::GenbankMap - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Mascot::GenbankMap;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::Mascot::GenbankMap, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
