package CDK::ThreadSafe::Iterator ; 

use strict;
use threads;
use threads::shared; 
require CDK::ThreadableObject; 

our @ISA = qw/CDK::ThreadableObject/;
our $PKG = 'CDK::ThreadSafe::Iterator'; 
 
sub new {
   my $class = shift; 
   my $list; 
   if (@_ == 1 and $_[0] eq 'ARRAY') { 
      $list = shift; 
   }else { 
    @{$list} = @_;
   }
   my $self = $class->SUPER::new('LIST'=>$list, 'NEXT'=>1); 
   $self->{'NEXT'} = 0 unless scalar @{$list} >0;
   return $self;
}

sub hasNext {
   my $self = shift; 
   return $self->{'NEXT'};
}
sub next {
   my $self = shift; 
   lock $self; 
   my $next = shift @{ $self->{'LIST'} };
   if ( scalar @{ $self->{'LIST'} } >0 ){
      # cool, we have more stuff
   }else {
      $self->{'NEXT'} = undef; 
   }
   return $next;
}

1; #it true ! 

