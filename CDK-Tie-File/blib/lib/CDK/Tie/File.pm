package CDK::Tie::File;

use strict;
use threads; 
use threads::shared; 
use Symbol; 


our $PKG : shared = "CDK::Tie::File"; 

sub DEBUG () {0}; 
sub OffsetCache () { return 0 } ;
sub FileHandleIndex () { return 1} ;
sub FileName ()	{return 2}; 
sub LineCount () {return 3}; 
sub TIEARRAY { 
   my $pkg = shift; 
   my $file = shift; 
   #my $self : shared = &share([]); 
   my $self = []; 
   my $fh = gensym(); 
  warn "$PKG: $fh is fh " if DEBUG; 
   { no strict qw(refs); 
      open($fh, "$file") or die "$PKG: $!: failed to open $file\n"; 
   }
   $self->[OffsetCache] = &share([]); 
   push @{$self->[OffsetCache]},'0'; 
   $self->[FileHandleIndex] = $fh; 
   $self->[FileName] = $file; 
   bless $self, $pkg; 
}

sub FETCHSIZE {
  my $self = shift;
  my $file = $self->[FileName]; 
  my $lines;
  if ( $self->[LineCount] > 0 ){ 
     return  $self->[LineCount]; 
  }else { 
     $lines = `wc -l $file`; 
     $lines= -1 +$1 if $lines=~/(\d+)/; 
     $self->[LineCount] = $lines; 
  } 
  return  $self->[LineCount]; 
}

sub FETCH {
   my ($self, $want) = @_; 
   warn "received self:$self, want: $want\n" if DEBUG; 
   
   die "$PKG: wanted a self and an index" unless (defined $want && $self); 
   #check if we already wanted this want before 
   my $seen = $self->[OffsetCache];
   my $fh   = $self->[FileHandleIndex];
   warn "fh : $fh\n" if DEBUG; 
  { lock $seen; 
   if ($want > @{$seen}) { 
     $self->advanceTo($want); 
   }else {
      { no strict qw(refs); 
        seek($fh, $seen->[$want], 0); 
      }
   } 
   my $return = <$fh>; 
   chomp $return; 
   return $return; 
  }
}

sub STORE { 
   die "$PKG: READ ONLY FILE ACCESS, SORRY\n"; 
}

sub DESTROY{
   my $self = shift;
   my $fh = $self->[FileHandleIndex]; 
   close $fh; 
}

sub advanceTo { 
   my($self, $want) = @_; 
   my $seen = $self->[OffsetCache]; 
 { lock $seen; 
   my $last_seen = @{$seen} -1 ;
   my $last_offset_value = $seen->[$last_seen]; 
   my $fh = $self->[FileHandleIndex];
   { no strict qw(refs); 
      seek ($fh, $last_offset_value, 0);
   }
   my $buffer; 
   while( defined ($buffer=<$fh>) ){
      $last_offset_value = length($buffer) + $last_offset_value; 
      $last_seen = $last_seen + 1; 
      { lock $seen; 
        push @{$seen}, $last_offset_value;
      }
      last if $last_seen > $want; 
   }
 }

}



1; # IT TRUE,IT MAGIC, IT ALWAYS TRUE 
