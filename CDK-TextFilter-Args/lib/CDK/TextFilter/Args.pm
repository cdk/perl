package CDK::TextFilter::Args;

use strict;
use warnings;
use CDK::Eland::Indexes qw/ $Matched_Contig
			    $Match_Position
			    $Match_Strand
			     /; 
require Exporter;

our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw(
		ELAND_TO_SGR_ARGS
	 	ELAND_TO_SGR_FWD_ONLY
		ELAND_TO_SGR_REV_ONLY
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
our $PKG = 'CDK::TextFilter::Args'; 
our %ELAND_TO_SGR_ARGS =	('splitOn'			=>"\t",
				 'keepColumns'			=>[ $Matched_Contig, $Match_Position, $Match_Strand], 
				 'comparisonType'		=>'Equal', 
				 'matchOnColumn'		=> $Matched_Contig, 
				 );  	

sub ELAND_TO_SGR_ARGS { return wantarray ? %ELAND_TO_SGR_ARGS : \%ELAND_TO_SGR_ARGS }

# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::TextFilter::Args - Perl extension for default CDK::TextFilter configuraton

=head1 SYNOPSIS

  use CDK::TextFilter::Args qw/ ELAND_TO_SGR /;
  my %options = ELAND_TO_SGR_ARGS; 
  $options{'matchThis'} = "string"; 
  my $textFilter = CDK::TextFilter->new(\%options); 
  while(<>){ 
     if ($textFilter->($_) ne "" ) { 
         #do something with $_
     }
  }

=head1 DESCRIPTION

This is just a way to create a hash with some default values to be used when creating CDK::TextFilter objects

=head2 EXPORT

None by default.
ELAND_TO_SGR_ARGS
ELAND_TO_SGR_FWD_ONLY
ELAND_TO_SGR_REV_ONLY


=head1 SEE ALSO

CDK::TextFilter
CDK::FilterFactory
CDK::FilteredFileReader

=head1 AUTHOR

cameron kennedy, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy

=cut
