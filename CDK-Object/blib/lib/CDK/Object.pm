package CDK::Object;

use strict;
use warnings;
no warnings qw/uninitialized numeric/;
use Carp; 
use AutoLoader ;
use Scalar::Util qw/looks_like_number/; 
use vars qw/$AUTOLOAD $VERSION $PKG $CLOBBER_OK/; 

$VERSION = '0.01';
$PKG = 'CDK::Object'; 
$CLOBBER_OK = 1; 

our $DEBUG = 0; 
sub DEBUG 	{ warn "$PKG: @_\n" if $DEBUG}
sub DEBUG_ON 	{ $DEBUG=1} 
sub DEBUG_OFF 	{ $DEBUG=undef} 


#Capital letters get autoloaded . . . slow, but easy. 
sub AUTOLOAD {
    #$AUTOLOAD will be something like FOO::Bar::Baz::OpenSesame
    #FOO::Bar::Baz being the package, and OpenSesame being the method or function desired
  my($pack,$func_name) = $AUTOLOAD=~/(.+)::([^:]+)$/;
  my $self = shift;
  croak "$PKG: only methods/properties starting with Capital letters are autoloaded. YOU sent : $pack, $func_name, @_\n"
    unless $func_name =~ /^[A-Z]/;
  return $self->generic($func_name,@_);
}

sub DESTROY { 
    my $self = shift; 
    undef %{$self}; 
}
#private sub, called by AutoLoader  
sub generic { 
    #we get handed a property value, and if we don't have @_
    #we assume that the caller wants to retrieve a value, as opposed to
    #undefining the value of the property.  We return simple values, and then 
    #examine weather or not the caller wants some type of array for reference values.
    #
    #if we are handed @_, we set the value, unless it is already set. 
    #if it is already set, we check to see if we can clobber it before continueing. 
    #if it exists, and we can clobber it, we do so.  Otherwise we return the current value
    #and issue a warning to guide usage. 
   my $self = shift; 
   my $prop = shift; 
   DEBUG "$PKG: No property in generic\n" unless $prop; ; 
   unless (@_ ) { 
	return $self->{$prop} unless ref($self->{$prop}); 
	if (ref($self->{$prop}) eq 'ARRAY' ) { 
		return wantarray ? @{ $self->{$prop} } : $self->{$prop}; 
	}elsif (ref($self->{$prop}) eq 'HASH'){ 
		return wantarray ? %{ $self->{$prop} } : $self->{$prop}; 
	}else{
		return $self->{$prop}; 
	}
   } ;
   if (! exists $self->{$prop}) { 
	$self->set($prop, @_); 
   }elsif (exists $self->{$prop} && $CLOBBER_OK ) { 
	$self->set($prop, @_); 
   }else { 
	DEBUG "$PKG: autoload generic sub refusing to clobber data, set $CDK::Object::CLOBBER_OK to change this\n";
	return $self->{$prop};
   }
}

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my $self = {}; 
   while (my $prop = shift @_){
      	if (my $val = shift @_ ){
      	 	$self->{$prop}  = $val; 
      	}else {
        	DEBUG "$PKG: I think you forgot to send in a value for $prop\n";
      	}
   }
   return bless ($self, $class);
}

sub pushOn { 
    #here we add elements to an array contained within the object. 
    #if the property is an array, we add to it. 
    #if it doesn't exist we create it. 
    #if it exists, and it isn't an array, and it is a scalar, we convert it to an array, if CLOBBER_OK
   my $self = shift; 
   my $prop = shift; 
   if (ref($self->{$prop}) eq 'ARRAY') {
      while (my $val = shift){ 
         DEBUG "$PKG: adding $val to $self->{$prop}\n";
         push @{ $self->{$prop} }, $val;
      }
   }elsif (! defined($self->{$prop}) ){ #NOT defined, create it and be happy 
	$self->{$prop} = [];
	$self->pushOn($prop, @_); 
   }elsif ($CLOBBER_OK) { 
        if (ref($self->{$prop}) eq 'HASH'){
            #issue warning, clobber
            DEBUG "$PKG: clobber $prop in $self, was HASH, now ARRAY !!! What are you doing ?\n";
            $self->{$prop}=[];
            $self->pushOn($prop, @_); 
        }
	#clobber existing value 
	$self->{$prop} = [$self->{$prop}, @_]; 
	#$self->pushOn($prop, @_);
   }else {
      DEBUG "$PKG: sub pushOn expect array reference, not $self->{$prop}, " . 
ref($self->{$prop}). " okay ? Set CDK::Object::CLOBBER_OK to prevent this message from being displayed\n"; 
   }
}

sub set { 
   my $self = shift;  
   if ( my $prop = shift){ 
      if (my $val = shift){
         $self->{$prop} = $val;
	 return $self->{$prop}; 
      }else { 
         $self->{$prop} = undef; 
	 return $self->{$prop};
      }
   }else { 
      DEBUG "$PKG: no idea why set was called without property and value\n"; 
      my ($package, $filename, $line) = caller;
      DEBUG "$PKG: package:$package, file:$filename, line:$line\n";
   }
}

sub get { 
   my $self = shift; 
   my $prop = shift; 
   if (ref($self->{$prop}) eq 'ARRAY'){
	return wantarray ? @{ $self->{$prop} } : $self->{$prop}; 
   }elsif (ref($self->{$prop}) eq 'HASH' ) { 
	return wantarray ? %{ $self->{$prop} } : $self->{$prop}; 
   }
   return $self->{$prop}; 
}

sub remove {
	my $self = shift; 
  	my $prop = shift; 
	return undef unless $prop; 
	delete $self->{$prop};
	return 1; 
}

sub hasProp{ 
   my $self = shift; 
   my $prop = shift; 
   return exists $self->{$prop} ? 1 : 0; 
}

sub propertiesList { 
	my $self = shift; 
	my $ret = CDK::List->new( keys %{$self} ); 
    return $ret; 
}

sub properties { 
	my $self = shift; 
	return wantarray ? keys %{$self} : [ keys %{$self} ] ; 
}

sub searchable_properties { 
   my $self = shift; 
   return wantarray ? keys %{ $self }  : [ keys %{ $self } ] ; 
} 

sub search { 
	my $self = shift; 
	my $want = shift; 
	my @properties = $self->searchable_properties(); 
	foreach my $key (@properties){ 
		if ($key eq $want) { 
			#we are looking at two identical strings
			#the key matches. Would better to use $obj->hasProp('foo')	
			return 1;
		}else{
			my $val = $self->get($key);  
			next unless $val; 
			if ($val eq $want){
				#string value of property is equal to $want
				#or are references to same memory address
				return 1; 
			}elsif (looks_like_number($val) && $val == $want){ 
				#could be a reference to the same object, or mathematically equal
				return 1; 
			}elsif (ref($val)){ 
				#we have a reference: could be CODE, HASH, ARRAY, SCALAR, or OtherObjectType
				my $type = ref($val); 
				if ( $type eq 'HASH') { 
					my $ret = _search_hash($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'ARRAY' ) { 
					my $ret = _search_array($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'SCALAR' ) { 
					return 1 if ${$val} eq $want; 
					return 1 if ${$val} == $want and looks_like_number(${$val}); 
				}elsif ($type eq 'CODE') { 
                    warn "Searching references of type CODE is not implemented\n"; 
                    return undef; 
                }else { 
					#we have an object I think.  
					if ($val->isa('CDK::Object')){ 
						#it is one of mine 
						my $ret = $val->search($want); 
						return $ret if $ret > 1;
					}elsif ($val->isa('HASH')){
						my $ret = _search_hash($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('ARRAY')){ 
						my $ret = _search_array($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('SCALAR')){ 
						return 1 if ${ $val } eq $want; 
						return 1 if ${ $val } == $want; 
					}else { 
							DEBUG("no idea what to do with $val in search"); 
					}
				}
			} # end reference searching else clause
		}# end value searching else clause
	} #end forech properties
	#we made it to the end
	#confess we found nothing
	return undef;  
}

sub _search_hash { 
	my $href = shift; 
	my $want = shift; 
	foreach my $key (keys %{ $href }){
		return 1 if $want eq $key; #same 
		my $val = $href->{$key}; 
		if (looks_like_number($val)){
			return 1 if $val == $want; 
		}elsif ($val eq $want) { 
			return 1; 
		}elsif(ref($val)){ 
				#we have a reference: could be CODE, HASH, ARRAY, SCALAR, or OtherObjectType
				my $type = ref($val); 
				if ( $type eq 'HASH') { 
					my $ret = _search_hash($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'ARRAY' ) { 
					my $ret = _search_array($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'SCALAR' ) { 
					return 1 if ${$val} eq $want; 
					return 1 if ${$val} == $want and looks_like_number(${$val}); 
				}elsif ($type eq 'CODE'){ 
                    warn "searching CODE references not implemented in CDK::Object\n"; 
                    return undef;
                }else { 
					#we have an object I think.  
					if ($val->isa('CDK::Object')){ 
						#it is one of mine 
						my $ret = $val->search($want); 
						return $ret if $ret >0;
					}elsif ($val->isa('HASH')){
						my $ret = _search_hash($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('ARRAY')){ 
						my $ret = _search_array($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('SCALAR')){ 
						return 1 if ${ $val } eq $want; 
						return 1 if ${ $val } == $want; 
					}else { 
							DEBUG("no idea what to do with $val in search"); 
					}
				}
		} # end reference searching else clause
	} #end foreach 
	#we made it to the end
	return undef
}

sub _search_array { 
	my $aref = shift; 
	my $want = shift; 
	foreach my $val (@{ $aref } ){
		if (looks_like_number($val)) { 
			return 1 if $val == $want; 
		}elsif ($val eq $want ) { 
			return 1; #equal strings or objects
		}elsif (ref($val)){
				#we have a reference: could be CODE, HASH, ARRAY, SCALAR, or OtherObjectType
				my $type = ref($val); 
				if ( $type eq 'HASH') { 
					my $ret = _search_hash($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'ARRAY' ) { 
					my $ret = _search_array($val, $want); 
					return $ret if $ret >0; 
				}elsif ( $type eq 'SCALAR' ) { 
					return 1 if ${$val} eq $want; 
					return 1 if ${$val} == $want and looks_like_number(${$val}); 
				}elsif ($type eq 'CODE') { 
                    warn "searching CODE references not implemented in CDK::Object\n"; 
                    return undef; 
                }else { 
					#we have an object I think.  
					if ($val->isa('CDK::Object')){ 
						#it is one of mine 
						my $ret = $val->search($want); 
						return $ret if $ret > 0;
					}elsif ($val->isa('HASH')){
						my $ret = _search_hash($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('ARRAY')){ 
						my $ret = _search_array($val, $want); 
						return $ret if $ret >0; 
					}elsif ($val->isa('SCALAR')){ 
						return 1 if ${ $val } eq $want; 
						return 1 if ${ $val } == $want; 
					}else { 
							DEBUG("no idea what to do with $val in search"); 
					}
				}
		} # end reference searching else clause
	} #end forech properties
	#we made it to the end
   return undef; 
}


1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Object - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Object;
  my $obj = CDK::Object->new(); 
  $obj->set('Name' => 'John Doe'); 
  $obj->set('Address' => { 'Street'=> "102 Elm St",
			   'Town'  => "Bezelburg", 
			   'Zip'   => "34427" } ); 

  #properties with capital letter can be autogenerated
  $obj->Age(25); 
  $name = $obj->Name; 
=head1 DESCRIPTION

 Just a generic object. 
 $obj = CDK::Object->new(); 
 $obj->set('key'=>'value'); 
 $obj->get('key'); 
 do {magic()} if $obj->hasProp('key'); 
 $obj->remove('key'); 
 my $propList = $obj->propertiesList(); 
 for ($i = $propList->getIterator; $i->hasNext(); ) { 
	my $prop = $i->next(); 
	$val = $obj->get($Prop); 
 } 
 my @properties = $obj->properties(); 
 print join "\n", map { "$_ => $obj->get($_)"} @properties; 
 Blah blah blah.
 
 #oh yeah, 
 #one more thing
 $has = $object->search("String"); 
 if ($has) { 
	#do something appropriate
 }
 $has = $obj->search(214545243534524365436); 
 if ($has){
	#somewhere in $obj, that number exists, either as a hash key string, 
	#as the value of a hash key string, as the name of an object's property, 
	#or as the value of an objects property. 
	#
	#it contains a big number somewhere
 }
 
 Ideally the search function would return something more useful, 
 but I don't know what that would be at this time. 
 Maybe something like if it  finds a  string, return the string.. 
 #keyboard battery    dead. Unfinishedehre. 
=head2 EXPORT

None by default.



=head1 SEE ALSO

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
