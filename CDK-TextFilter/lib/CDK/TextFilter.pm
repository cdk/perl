package CDK::TextFilter;

use 5.010000;
use strict;
use warnings;

our $VERSION 	= '0.01';
our $PKG	= 'CDK::TextFilter'; 

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my $args = shift; 
   my $self = _composeFilter($args); 
   return bless $self, $class; 
}

sub _composeFilter { 
     my $args = shift;
     my %args = %{$args};  
     my $splitChar = $args{'splitOn'}; 
     my $wantSplit = $splitChar ? 1 : 0; 
     my @keepCols  = @{ $args{'keepColumns'}}; 
     my $rejoin    = $args{'rejoin'}; 
     my $joinWith  = $args{'reJoinWith'}; 
     my $want 	   = $args{'matchThis'}; 
	$want ||= qr/.*/; 
     my $no_want   = $args{'noMatchThis'}; 
	$no_want ||= qr/9999_99999_99999_999_AAAAAAAA_ZZZZZ_999909099/; 
     my $matchCol  = $args{'matchOnColumn'}; 
     my $returnWholeLine = $args{'returnOriginalLine'}; 
     my $comparisonType = $args{'comparisonType'} || "" ; 
 
     my $filter = sub { 
	my $line = shift; 
        my @split; 
     	if ($wantSplit) { 
	   @split = split $splitChar, $line; 
 	   if ( $comparisonType eq 'Match' ) { 
		if ($split[$matchCol] =~/$want/ and $split[$matchCol] !~/$no_want/ ) { 
		    if ($returnWholeLine) { return $line } 
		    if ($rejoin) { return join $joinWith, (@split[@keepCols]) } 
		    if (@keepCols) { return wantarray ? @split[@keepCols] : \@split[@keepCols] } 
		    return 1; 
		}
	    }elsif ($comparisonType eq 'Equal' ) { 
		if ($split[$matchCol] eq $want ){
		    if ($returnWholeLine) { return $line } 
		    if ($rejoin) { return join $joinWith, @split[@keepCols] } 
		    if (@keepCols) { return wantarray ? @split[@keepCols] : \@split[@keepCols] } 
		    return 1; 
		}
	    }else { #we have no match type, just return the columns they want
		if ($returnWholeLine){return $line}
		if ($rejoin) { return join $joinWith, @split[@keepCols]}
		if (@keepCols){ return wantarray ? @split[@keepCols] : \@split[@keepCols] }
		#I guess we have no match type, and they don't want anything but truth back, always 1
		return 1; 
	    }  
	}else { 
           warn "no split checking $line for $want\n";
	   if ($comparisonType eq 'Match'){
		if ($line=~/$want/ and $line !~/$no_want/){
		   return $line if $returnWholeLine;
		   return 1; 
		}
	   }elsif ($comparisonType eq 'Equal' ) { 
		if ($want eq $line ) {
		    return $line if $returnWholeLine; 
		    return 1; 
		}
	   }else { # no matching, no splitting, just return either the line or 1 as appropriate
		return $line if $returnWholeLine;
		return 1; 
	  }
	   
       }
       return undef; #at this point, we are out of options, failed.  
   };
  
   return $filter;  
	
}	

# Autoload methods go after =cut, and are processed by the autosplit program.

1; # IT MAGIC, IT TRUE, Don't delete the one whatever you do
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::TextFilter - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::TextFilter;
  my $filter = CDK::TextFilter->new( \%config ); 
  @return = $filter->("string"); 
  $val    = $filter->("test"); 
  ...  
  while(<>) { 
      if ($filter->($_)){
 	  #do something with $_
      }
  }

=head1 DESCRIPTION

A subroutine that processes @_ and returns context dependant TRUE or UNDEF depending on the outcome of the expressions in the subroutine.

This module accept a hash a parameters 

$hash = { 	
		'splitOn' 	=> VALUE,

	  	'keepColumns'	=>[ARRAY],

		'rejoin'	=> 1, # or zero 

		'reJoinWith'	=> VALUE,

		'matchThis'	=> MATCH_STRING,

		'matchOnColumn'	=> INDEX_VALUE,

		'returnWholeLine'=> 0, # or 1 

		'comparisonType' => Match # or Equal ( Match contains the MATCH_STRING, Equal IS the MATCH_STRING

     	}; 

		
=head2 EXPORT

none, OO module.  


=head1 SEE ALSO

CDK::TextFilter::Args
CDK::FilterFactory
CDK::FilteredFileReader

=head1 AUTHOR

cameron kennedy, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy


=cut
