package CDK::Eland::Indexes;

use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

use Readonly; 
our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw(
	$Machine
	$Run_Numer
	$Lane
	$Tile
	$X_Coordinate_Of_Cluster
	$Y_Coordinate_Of_Cluster
	$Index_String
	$Read_Number
	$Read
	$Quality_String
	$Matched_Chromosome
	$Matched_Contig
	$Match_Position
	$Match_Strand
	$Match_Descriptor
	$Single_Read_alignment_Score
	$Paired_Read_alignment_Score
	$Partner_Chromosome
	$Partner_Contig
	$Partner_Offset
	$Partner_Strand
	$Filtering 

) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
Readonly our $Machine 				=> 0; 
Readonly our $Run_Number	 		=> 1;
Readonly our $Lane				=> 2;
Readonly our $Tile				=> 3; 
Readonly our $X_Coordinate_Of_Cluster		=> 4;
Readonly our $Y_Coordinate_Of_Cluster		=> 5; 
Readonly our $Index_String			=> 6; 
Readonly our $Read_Number			=> 7;
Readonly our $Read				=> 8;
Readonly our $Quality_String			=> 9; 
Readonly our $Matched_Chromosome 		=>10; 
Readonly our $Matched_Contig			=>11; 
Readonly our $Match_Position			=>12; 
Readonly our $Match_Strand			=>13; 
Readonly our $Match_Descriptor			=>14; 
Readonly our $Single_Read_alignment_Score	=>15; 
Readonly our $Paired_Read_alignment_Score	=>16; 
Readonly our $Partner_Chromosome		=>17; 
Readonly our $Partner_Contig			=>18; 
Readonly our $Partner_Offset			=>19; 
Readonly our $Partner_Strand			=>20; 
Readonly our $Filtering 			=>21; 

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Eland::Indexes - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Eland::Indexes;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::Eland::Indexes, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
