package CDK::Mascot::ProteinHitListGroup;

use strict;
use warnings;
use CDK::List; 
our @ISA = qw(CDK::List);

our $VERSION = '0.01';
our $PKG = 'CDK::Mascot::ProteinHitListGroup'; 

my $DEBUG = 0;
sub DEBUG { warn "$PKG : @_ \n" if $DEBUG} 
sub DEBUG_OFF { $DEBUG=undef}
sub DEBUG_ON  { $DEBUG = 1 } 

sub add { 
   #adds protein_hit_list objects to the protein_hit_group
   my $self = shift; 
   my $phl = shift; 
   if ($phl->isa('CDK::Mascot::ProteinHitList')){
	$self->SUPER::add($phl);
   }else { 
	warn "$PKG: expecting CDK::ProteinHitList and found $phl\n"
   }
}


1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::ProteinHitGroup - MODULE to hold a list of ProteinHitList objects

=head1 SYNOPSIS

  use CDK::Mascot::ProteinHitGroup;
  my $phl = CDK::ProteinHitList->new(); 
  $phl->add($proteinHit); 
  $phg = CDK::Mascot::ProteinHitGroup->new(); 
  $phg->add($phl); 
   #get all the protein hit lists in the protein hit group as array
   my @phls = $phg->getArray(); 
   
   #or iterate through the protein hit lists
   for($itr=$phg->getIterator(); $itr->hasNext;){
	my $phl = $itr->next(); 
   }
   #etc 

=head1 DESCRIPTION

A CDK::List by any other name ... just a collection of ProteinHitList objects. 
Subclass of CDK::List;  Used in CDK::MascotCSV::File to manage collection of Protein Hit Lists

=head2 EXPORT

None by default.


=head1 SEE ALSO

CDK::List CDK::MascotCSV::File CDK::Mascot::ProteinHitList CDK::Mascot::ProteinHit CDK::Mascot::PeptideHitList CDK::Mascot::PeptideHit 

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
