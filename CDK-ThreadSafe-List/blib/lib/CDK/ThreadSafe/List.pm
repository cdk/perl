package CDK::ThreadSafe::List ; 

use strict; 
use threads; 
use threads::shared qw/share is_shared/;
use Scalar::Util qw/reftype blessed/; 
require CDK::ThreadableObject; 
use CDK::ThreadSafe::Iterator;    
our $VERSION = 0.01; 
our $PKG = 'CDK::ThreadSafe::List';
   
sub new { 
   my $class = shift; 
   my $self = &share([]); 
   if (scalar @_ == 1 && ref($_[0] eq'ARRAY')){
      $self = _make_shared(shift @_);
   }
   bless ($self, $class);
   $self->add(@_) if @_; 
   return $self; 
}

sub add { 
   my $self = shift; 
   lock $self; 
   foreach my $arg (@_){ 
      push @{$self}, _make_shared($arg);
   }
}

sub count { 
   my $self = shift; 
   return scalar @{$self}; 
}

sub getArray { 
   my $self = shift; 
   return @{$self};
}

sub getIterator {
   my $self = shift; 
#   return CDK::ThreadSafe::Iterator->new(@{$self}); 
   return CDK::ThreadSafe::Iterator->new($self); 
}

sub _make_shared {
   my $test = shift; 
   #if shared, return
   return $test if is_shared($test);
   #otherwise, we return this
   my $return = undef;
   #normally, we receive reference of the following types
   if (my $ref_t = reftype($test)){
      if ($ref_t eq 'ARRAY'){
         $return = &share([]); 
         foreach my $item (@{$test}){
            push @{$return}, _make_shared($item); 
         }
       }elsif( $ref_t = 'HASH'){
          $return = &share({}); 
          foreach my $k (%{$test}){
             $return->{$k} = _make_shared($test->{$k});
          }
       }elsif ($ref_t = 'SCALAR'){ 
          $return = \do{ my $scalar = $$test; }; 
       }
    
       if ($return ) { 
           #check if has readonly flag set, and handle
          if (Internals::SvREADONLY($test)) { 
              Internals::SvREADONLY($return, 1);
          }
          #if the thing is blessed, then bless the return
          if ( my $class = blessed($test) ) { 
             bless($return, $class);
          }
          #give it back
          return $return; 
       }
    }
    #hope this is just a simple scalar value, 
    #since we have no reference, we just return it
    return $test ;
}

1; # Its true ! 