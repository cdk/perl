package CDK::Iterator ;
use strict;
use lib '/users/cameron/src/perlib/';

sub new {
   my $class = shift; 
   my @list = @_;
   my $self = { 'LIST'=>\@list,
		'NEXT'=>'1' };
   bless $self, $class;  
   $self->{'NEXT'} = 0 unless scalar @list >0;

   return $self;
}
sub hasNext {
   my $self = shift; 
   return $self->{'NEXT'};
}
sub next {
   my $self = shift; 
   my $next = shift @{ $self->{'LIST'} };
   if ( scalar @{ $self->{'LIST'} } >0 ){
      # cool, we have more stuff
   }else {
      $self->{'NEXT'} = undef; 
   }
   return $next;
}
1;

