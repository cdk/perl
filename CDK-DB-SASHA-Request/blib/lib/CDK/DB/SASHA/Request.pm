package CDK::DB::SASHA::Request;

use strict;
use DBI;
our $VERSION = 1.0;
our $DEBUG = undef;
our $PKG = 'CDK::DB::SASHA::Request';
our $DBH = DBI->connect("dbi:mysql:sasha;mysql_read_default_file=$ENV{HOME}/.my.cnf");

our $ORGANISM_ID_REQUEST 	= $DBH->prepare("SELECT id from organism WHERE name=?"); 
our $ADD_ORGANISM_REQUEST 	= $DBH->prepare("INSERT INTO organism SET name=?");
our $ADD_TRACE_REQUEST    	= $DBH->prepare("INSERT INTO traces SET name=?, organism_id=?, bases=?");
our $GET_TRACE_ID_REQUEST 	= $DBH->prepare("SELECT id from traces WHERE organism_id=? AND name=?");
our $GET_TRACE_NAME_REQUEST 	= $DBH->prepare("SELECT name from traces WHERE id=?");
our $GET_LAST_TRACE_ID_REQUEST 	= $DBH->prepare("SELECT max(id) from traces WHERE organism_id=?");
our $CHECK_XMER_REQUEST   	= $DBH->prepare("SELECT id FROM xmers where sequence=?");
our $ADD_XMER_REQUEST 		= $DBH->prepare("INSERT INTO xmers SET sequence=?");
our $GET_XMER_REQUEST 	 	= $DBH->prepare("SELECT sequence FROM xmers where id=?");
our $GET_COUNT_ID_REQUEST 	= $DBH->prepare("SELECT count_id from counts where organism_id=? AND trace_id=? AND xmer_id=?");
our $NEW_COUNT_ID_REQUEST 	= $DBH->prepare("INSERT INTO counts (organism_id, trace_id, xmer_id,count) VALUES(?,?,?,?)");
our $GET_XMER_COUNT_REQUEST 	= $DBH->prepare("SELECT count from counts where xmer_id=? AND trace_id=? AND organism_id=?");
our $UPDATE_COUNT_REQUEST 	= $DBH->prepare("UPDATE counts SET count=count+1 WHERE organism_id=?, trace_id=?, xmer_id=?");
our $UPDATE_BY_COUNT_ID_REQUEST	= $DBH->prepare("UPDATE counts SET count=count+1 WHERE count_id=?");
our $SET_COUNT_BY_ID_REQUEST	= $DBH->prepare("UPDATE counts SET count=? WHERE count_id=?");
our $ADD_CLIP_REQUEST	 	= $DBH->prepare("INSERT INTO clip_positions SET trace_id=?, start=?, end=?");
our $GET_CLIP_START_REQUEST 	= $DBH->prepare("SELECT CLIP_LEFT from ancfiledata WHERE TI=?");
our $GET_CLIP_END_REQUEST 	= $DBH->prepare("SELECT CLIP_RIGHT from ancfiledata WHERE TI=?");
our $ADD_ANC_ENTRY_REQUEST 	= $DBH->prepare("INSERT INTO ancfiledata (ACCESSION, AMPLIFICATION_FORWARD, AMPLIFICATION_REVERSE, AMPLIFICATION_SIZE, ASSEMBLY_ID, CENTER_NAME, CENTER_PROJECT, CHEMISTRY, CHEMISTRY_TYPE, CHIP_DESIGN_ID, CHROMOSOME, CHROMOSOME_REGION, CLIP_LEFT, CLIP_QUALITY_LEFT, CLIP_QUALITY_RIGHT, CLIP_RIGHT, CLIP_VECTOR_LEFT, CLIP_VECTOR_RIGHT, CLONE_ID, COMPRESS_TYPE, CULTIVAR_GROUP, CVECTOR_ACCESSION, CVECTOR_CODE, ENVIRONMENT_TYPE, HI_FILTER_SIZE, HOST_CONDITION, HOST_ID, HOST_LOCATION, HOST_SPECIES, INDIVIDUAL_ID, INSERT_SIZE, INSERT_STDEV, ITERATION, LIBRARY_ID, LO_FILTER_SIZE, PICK_GROUP_ID, PLACE_NAME, PLATE_ID, POPULATION_ID, PREP_GROUP_ID, PRIMER, PRIMER_CODE, PROGRAM_ID, REFERENCE_ACCESSION, REFERENCE_OFFSET, RUN_DATE, RUN_GROUP_ID, RUN_LANE, RUN_MACHINE_ID, RUN_MACHINE_TYPE, SEQ_LIB_ID, SOURCE_TYPE, SPECIES_CODE, STRATEGY, SUBMISSION_TYPE, SUBSPECIES_ID, SVECTOR_ACCESSION, SVECTOR_CODE, TEMPLATE_ID, TI, TRACE_DIRECTION, TRACE_END, TRACE_FORMAT, TRACE_NAME, TRACE_TYPE_CODE, TRANSPOSON_ACC, TRANSPOSON_CODE, WELL_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
 
sub new { 
	my $that = shift;
	my $class = ref($that) || $that;
	my $self = {};
	return bless $self, $class;
}

sub setAutoCommit {
   my $self = shift; 
   my $value = shift;
   (warn "invalid value" and return ) unless $value=~/^(1|0)$/; 
   $DBH->{AutoCommit} = $value; 
}
sub begin {
   my $self = shift; 
   $DBH->begin_work();
}
sub commit {
   my $self= shift; 
   $DBH->commit();
}
sub rollback {
   my $self = shift; 
   $DBH->rollback();
}
sub addOrganism {
	my $self = shift;
 	my $rc = $ADD_ORGANISM_REQUEST->execute($_[0]);
 	return $rc;
}

sub getOrganismID {
   my $self = shift;
   my $name = shift;
   $ORGANISM_ID_REQUEST->execute($name);
   my $data = $ORGANISM_ID_REQUEST->fetchall_arrayref();
   DEBUG("data is $data : @{$data}\n");
   DEBUG("too many items in a unique list") if scalar @{$data} >1 ;
   DEBUG ("too few items in a unique list") if scalar @{$data} <1;
   my $id = $data->[0]->[0];;
   DEBUG ("return organism ID is $id \n");
   return $id;
}

sub addTrace {
   my $self = shift;
   my $args = shift;
   DEBUG ("expect HASH reference in addTrace not $args\n") unless ref($args) eq 'HASH';
   my $ret = $ADD_TRACE_REQUEST->execute($args->{'-name'}, $args->{'-organism_id'}, $args->{'-bases'});
   DEBUG ("addTrace returned $ret\n");
   return $ret;
}
sub getLastTraceName {
   my $self = shift; 
   my $organism = shift; 
   $GET_LAST_TRACE_ID_REQUEST->execute($organism); 
   my $data = $GET_LAST_TRACE_ID_REQUEST->fetchrow_arrayref();
   my $id = $data->[0];
   my $name = $self->getTraceNameByID($id);
   return $name;
}
sub getTraceNameByID {
   my $self = shift; 
   my $id = shift; 
   $GET_TRACE_NAME_REQUEST->execute($id);
   my $data = $GET_TRACE_NAME_REQUEST->fetchrow_arrayref();
   DEBUG ("trace name is $data->[0]");
   return $data->[0];
}
sub getTraceID {
   my $self = shift;
   my $args =shift;
   $GET_TRACE_ID_REQUEST->execute($args->{'-organism'}, $args->{'-name'});
   my $data = $GET_TRACE_ID_REQUEST->fetchrow_arrayref();
   return $data->[0];
}
sub getXmerHash {
   my $self = shift; 
   my $pattern = shift; 
   return $DBH->selectall_hashref("SELECT id, sequence FROM xmers", 'sequence') unless $pattern; 
   $pattern = $DBH->quote($pattern);
   return $DBH->selectall_hashref("SELECT id, sequence FROM xmers WHERE sequence REGEXP $pattern", 'sequence'); 
}
sub check4XMER {
   my $self = shift;
   my $sequence = shift;
   $CHECK_XMER_REQUEST->execute($sequence);
   my $id = $CHECK_XMER_REQUEST->fetchrow_arrayref();
   return $id->[0] || undef; 
}
sub addXMER {
   my $self = shift;
   my $sequence = shift;
   if($self->check4XMER($sequence)){
      #we have this already
	  warn "I already have this XMER, Do you know what you are doing ? \n";
   }else {
      $ADD_XMER_REQUEST->execute($sequence);
   }	  
}
sub getXMERSequence {
   my $self = shift;
   my $id = shift;
   $GET_XMER_REQUEST->execute($id);
   my $data = $GET_XMER_REQUEST->fetchrow_arrayref();
   return $data->[0];
}
sub newXMERCount {
   my $self = shift;
	if ( ref($_[0])  eq 'hash' ){
		my $r = $_[0];
	   $NEW_COUNT_ID_REQUEST->execute($r->{'-organism_id'}, $r->{'-trace_id'}, $r->{'-xmer_id'}, $r->{'-count'});
	}else {
    #warn join "\n", ("newXMERCount", @_);
    $NEW_COUNT_ID_REQUEST->execute($_[0], $_[1], $_[2], $_[3]);
	} 
}
sub bulkLoadXmers {
   my $self = shift; 
   my $data = shift;
   my $first = undef; #keeps track of if I need a comma or not
   my $query = "INSERT INTO counts (organism_id, trace_id, xmer_id,count) VALUES ";
   foreach my $d (@$data) { 
      $query .= ', ' if $first;
      $query .= '(';
      $query .= join ",", map{ $DBH->quote($_) } @{$d}; 
      $query .= ')';
      $first++;
   } #done building query string.  
   #warn "ready to execute query \n$query\n";
   $DBH->do($query) or die "bad query\n";
}

sub getXMERCount{
   my $self = shift;
   my $a = shift;
   die "$a is not a has\n" unless ref($a) eq 'HASH';
   $GET_XMER_COUNT_REQUEST->execute($a->{'-xmer_id'},$a->{'-trace_id'}, $a->{'-organism_id'});
   my $data = $GET_XMER_COUNT_REQUEST->fetchrow_arrayref();
   return $data->[0];
   
}
sub getCountID{
   my $self = shift;
   my $a = shift;
   DEBUG("org_id $a->{'-organism_id'}, trace_id $a->{'-trace_id'}, xmer_id $a->{'-xmer_id'}");
   $GET_COUNT_ID_REQUEST->execute($a->{'-organism_id'}, $a->{'-trace_id'}, $a->{'-xmer_id'});
   my $data = $GET_COUNT_ID_REQUEST->fetchrow_arrayref();
   return $data->[0];
}
sub updateCount {
   my $self = shift;
   my $args = shift;
   my $id = $self->getCountID($args);
   $self->updateCountByID($id);
}
sub incrementCountByID {
   my $self = shift;
   my $id = shift;
   #poorly named variable to follow
   $UPDATE_BY_COUNT_ID_REQUEST->execute($id);
}
sub setCountByID {
   my $self = shift;
   my $args = shift; 
   #commented out because this is easier, and works correctly
   #$SET_COUNT_BY_ID_REQUEST->execute($args->{-'value'}, $args->{'-id'});
   my $query = "UPDATE counts "; 
   $query .= "SET count=";
   $query .= $DBH->quote( $args->{'-value'} ); 
   $query .= " WHERE count_id=";
   $query .= $DBH->quote( $args->{'-id'} );
   DEBUG("query is $query"); 
   my $rc = $DBH->do($query);
   return $rc;
}

sub addClip {
   my $self = shift;
   my $args = shift;
   $ADD_CLIP_REQUEST->execute($args->{'-trace_id'}, $args->{'-start'}, $args->{'-end'});
}
sub getClipStart {
   my $self = shift; 
   my $trace_id = shift; 
   $GET_CLIP_START_REQUEST->execute($trace_id);
   my $data = $GET_CLIP_START_REQUEST->fetchrow_arrayref();
   DEBUG ("received $data for TI = $trace_id, returning $data->[0]\n");
   return $data->[0];
}
sub getClipEnd {
   my $self = shift;
   my $trace_id = shift;
   $GET_CLIP_END_REQUEST->execute($trace_id);
   my $data = $GET_CLIP_END_REQUEST->fetchrow_arrayref();
   return $data->[0];
}
sub addANCEntry {
   my $self = shift; 
   $ADD_ANC_ENTRY_REQUEST->execute(@_);
}
sub DEBUG {
	warn "$PKG :: $_[0]\n" if $DEBUG;
}

1; #it true

# POD documentation follows. 
=pod

=head1 NAME 
 
 CDK::DB::SASHA::Request
 
=head1 DESCRIPTION
 
 A Module to keep track of common request to SASHAS database
 
 =head1 SYNOPSIS
 
  #! /usr/bin/perl 
  use strict;
  use CDK::DB::SASHA::Request; 
  my $db = CDK::DB::SASHA::Request->new();
  my $db->addOrganism("Something Latin"); 
  my $org_id = $db->getOrganismID("Something Latin");
  
=head1 METHODS 

 addOrganism($organism) 
    a method to add an organism to the database
	$db->addOrganism("Foobarius Moebius");
	
 getOrganismID($organism_name) 
    a method to get the database ID for an organism
	 $id = $db->getOrganismID("Foobarius Meobius");

 addTrace ($hash_ref) 
    a method to add a trace to the database
	 takes a hash reference as an argument
	 returns '1' upon success 
	 $hash_ref = {'-name'=>$traceName, '-organism_id'=>$org_id, '-bases'=>$bases};
	 $rc = $db->addTrace($hash_ref);
	 
  getLastTraceName($organism_name)
	 a method to get the last trace added for a particular organism
	 takes a string identifying the organism as argument to function
	 returns NCBI 'TI' trace identifier for the last trace added
	 my $trace_name = $db->getLastTraceName("Foobarius Moebius");
	 
 getTraceNameByID($trace_id) 
    a method to get the name of a trace from its ID number in the database
	 $trace_name = $db->getTraceNameByID($trace_id);

 getTraceID($hash_ref)
    a method to get the ID of a trace when the organism and name are known
	 takes a hash_ref as an argument with keys for '-organism', and '-name'
	 $trace_id = $db->getTraceID({'-organism'=>$organism, '-name'=>$trace_name});
	 
 check4XMER($xmer_sequence)
    a method to check if an XMER is already present in the database
	 return XMER ID from database if the XMER is present
	 $xmer_id = $db->check4XMER($xmer_sequence);
	 # or use in conditional
	  if ($db->check4XMER($xmer)) { 
	     #do something appropriate
	  }
	  
	  
 addXMER($xmer_sequence)
    a method to add an XMER sequence to the database
	 $db->addXMER($xmer_sequence)
	 
 getXMERSequence($xmer_id)
    a method to get an XMER sequence by its xmer_id in the database
	 my $xmer_sequence = $db->getXMERSequence($xmer_id);
	 
 newXMERCount($hash_ref) || newXMERCount(@args) 
    a method to create a count entry for an XMER
	 takes a hash_reference as an arg, or an array
	 $args = {'-organism_id'=>$org_id, '-trace_id'=>$trace_id, '-xmer_id'=>$xmer_id, '-count'=>$count};
	 $db->newXMERCount($args);
	 #or use the more risky, and likely to change, and therefore not recommended array arguement. 
	 $db->newXERCount($organism_id, $trace_id, $xmer_id, $count);
	 
 getXMERCount($args)
    a method to get the count of a particular xmer and organism.
	 takes a hash_ref as args and returns count of xmer
	 $args = {'-xmer_id'=>$xmer_id, '-organism_id'=>$organism_id,'-trace_id'=>$ti };
	 $count = $db->getXMERCount($args);
	
 getCountID($args)
    a method to get the ID of an XMER count
	 takes a hash ref as argument, return ID of count
	 $args = {'-organism_id'=>$org_id, '-trace_id'=>$trace_id, '-xmer_id'=>$xmer_id};
	 $count_id = $db->getCountID($args);

 updateCount($args)
    a method to increment the count by one inthe database for an xmer
	 takes a hash reference as argument
	 $args = {'-organism_id'=>$org_id, '-trace_id'=>$trace_id, '-xmer_id'=>$xmer_id};
	 $db->updateCount($args);
	 
 incrementCountByID($count_id)
    a method to increment the count by one in the database for count_id  $count_id
    $db->incrementCountByID($count_id)
	 
 setCountByID($args)
    a method to set the count to a particular value
	 takes a hash_ref as an argument
	 $newCount = $oldCount+$number;
	 $args = {'-value'=>$newCount, '-id'=>$count_id};
	 $db->setCountByID($args);
	 
 addClip($args)
    a method to add a clip position to the database
	 takes hashref as an argument
	 $args = {'-trace_id'=>$trace_id, '-start'=>$start, '-end'=>$end};
	 $db->addClip($args);
	 
 getClipStart($trace_id)
    a method to get the start position of the clipping
	 my $start = $db->getClipStart($trace_id);

 getClipEnd($trace_id) 
    a method to get the end position of the clipping
	 my $end = $db->getClipEnd($trace_id);
	 
 addANCEntry(@data)
    a method to get ancillary data into the database
	 See CDK::ANCConstants for the contents of this array
	 my @data = $ancReader->getNextEntry();
	 $db->addANCEntry(@data);

=head1 Copyright

 Cameron D Kennedy, 2007
 All rights reserved, no gaurantee, no warranty
	 
=cut
 
1; #it's true ! 
