package CDK::FilterFactory;

use strict;
use warnings;
use CDK::List; 
use CDK::TextFilter; 
require Exporter;
require CDK::Iterator; 

our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw(
	createFilter
	createFilterList
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION = '0.01';
our $MODE = 'CODE'; 
our $PKG  = 'CDK::FilterFactory'; 
sub createFilter { 
   my $string = shift; 
   my $FilterArgs = shift; 
      $FilterArgs->{'matchThis'} = $string; 
      $FilterArgs->{'returnOriginalLine'} = ""; 
   return CDK::TextFilter->new($FilterArgs);
}

sub createFilterList { 
   my $list = shift; 
   my $FilterArgs = shift; 
   my $ret = CDK::List->new(); 
   warn "$PKG: I want a list !\n" unless ref($list) eq 'CDK::List'; 
   if (ref($list) eq 'CDK::List') { 
	warn "$PKG: making filter list for a LIST\n"; 
	#awesome, we list these
	my $itr = $list->getIterator(); 
	for ($itr=$list->getIterator(); $itr->hasNext(); ){
	   #my $next = $itr->next(); 
	   $FilterArgs->{'matchThis'} = $itr->next(); 
	   $ret->add(CDK::TextFilter->new($FilterArgs)); 	
	}
   }elsif (ref($list) eq 'ARRAY'){ 
	#OKAY, we can handle this too
	foreach (@{$list}){ 
		$FilterArgs->{'matchThis'} = $_; 
		$ret->add(CDK::TextFilter->new($FilterArgs)) ;
	}
   }else{ 
	#HAIL MARRY ! 
	foreach ($list, @_) { 
		$FilterArgs->{'matchThis'} = $_; 
		$ret->add(CDK::TextFilter->new($FilterArgs));
	}

   }
	
   return $ret; 
}
1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::FilterFactory - Perl extension for making regular expression filters

=head1 SYNOPSIS

  use CDK::FilterFactory qw/ createFilter /;
  my $filter = createFilter($string); 
  my $filterList = createFilterList(@array); 
  my $filterList = createFilterList($CDK::List); 
  my $filterList = createFilterList($array_reference); 
=head1 DESCRIPTION

You have strings, and want to make regular expressions, this is a factory to do just such a thing. 


=head2 EXPORT

None by default.
createFilter
createFilterList



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
