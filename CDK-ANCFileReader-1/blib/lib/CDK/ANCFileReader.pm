package CDK::ANCFileReader ; 
use strict;
our $VERSION = 1.0;
our $DEBUG = 1;

sub new {
   my $class = shift;
   my $self = {};
   bless $self, ref($class)||$class;
   my $fh = shift;
   if ($fh){
      $self->setFileHandle($fh);
   }
   return  $self;
}
sub setFileHandle{
   my $self = shift;
   $self->{'-filehandle'} = shift;
}
sub getFileHandle{
        my $self = shift;
        return $self->{'-filehandle'};
}
sub getNextEntry {
   my $self = shift;
   my $fh = $self->getFileHandle();
   my $line = <$fh>;
   return undef unless $line; 
   #DEBUG("line:$line\n");
   my @data = split(/\t/, $line) ;
   return wantarray ? @data : \@data;
}

sub DEBUG {print join "\n", @_ if $DEBUG};

# pod documentation follows

=head1 NAME
 
 CDK::ANCFileReader

=head1 SYNOPSIS
 
 use strict;
 use CDK::ANCFileReader; 
 use Symbol qw/gensym/;
 foreach my $file (@ARGV) { 
	my $fh = gensym(); 
	open($fh, $file) or die "bang dead at open : $!";
	my $reader = CDK::ANCFileReader->new($fh); 
	while(my $entry = $reader->getNextEntry()){
	   #do something with array reference $entry
	}
 }
 
=head1 METHODS

=item 1 B<setFileHandle($fh)>

=over 4 
 
 $reader->setFileHandle($fh);
 sets the reader to read from the open file handle $fh

=back
 
=item 2 B<getFileHandle()>

=over 4
 
 $fh = $reader->getFileHandle();
 returns the file handle currently being read from

=back
 
=item 3 B<getNextEntry()>

=over 4
 
 @ancData = $reader->getNextEntry();
 returns either an array, or an array reference depending on context
 containing the data for the next line in the ancillary data file
 see CDK::ANCConstants for easy indexing of the array

=back

=head1 Copyright

 Cameron D Kennedy, 2007
 All rights reserved, no guarantee, no warranties

=cut
1; #it true ! 

