package CDK::TandemRepeatThing::Util;
require Exporter; 
our @ISA = qw/Exporter/; 
use strict; 
use Clone qw/clone/; 
use CDK::Sequence::Util qw/compliment/; 
our $PKG = 'CDK::TandemRepeatThing::Util';
our %bias; 
our $MINIMUM_READS = 0; 

our @EXPORT_OK = qw(merge_reports 
                    xmers_by_count_ascending 
                    xmers_by_count_descending
		            xmers_by_percent_gc_ascending 
                    xmers_by_percent_gc_descending 
		            xmers_by_length_ascending 
                    xmers_by_length_descending 
		            xmers_by_percent_watson_ascending 
                    getWatsonPercentage
		            gcp 
                    xmers_by_default
                    percent_ideal
                    sequences
                    counts2percents
                    counts2percents_of_repeatunit); 
our %EXPORT_TAGS = ("all"=>[@EXPORT_OK]); 

sub merge_reports { 
   my $return = {}; 
   my $reports = shift @_; 
   foreach my $report (@{$reports})   {
      warn "report : $report " . ref($report)."\n"; 
      my @xmers = grep /^[ATGC]+$/, keys %{$report}; 
      foreach my $xmer (@xmers) { 
         next unless $report->{$xmer}->{'count'} > $MINIMUM_READS; 
         push @{ $return->{'xmers'} }, $xmer unless defined($return->{$xmer}); 
         $return->{$xmer}->{'count'} += $report->{$xmer}->{'count'}; 
         warn "count:$return->{$xmer}->{'count'} \n"; 
         my @sequences = grep {$_ !~/count/} (keys %{ $report->{$xmer} }) ; 
	 foreach my $seq (@sequences) { 
	    $return->{$xmer}->{$seq}->{'watson'} += $report->{$xmer}->{$seq}->{'watson'}; 
	    $return->{$xmer}->{$seq}->{'crick'} += $report->{$xmer}->{$seq}->{'crick'}; 
         }
      }
   }
   return bless $return , 'CDK::TandemRepeatThing::Report'; 
}

sub xmers_by_default { 
   my $self = shift; 
   my @ret = map { $_->[0] } sort {
				   length($a->[0]) <=> length($b->[0]) 
				            ||
				   $b->[1] <=> $a->[1]
					    ||
				   length($a->[0]) <=> length($b->[0]) 
				            ||
				   gcp($b->[0]) <=> gcp($a->[0]) 
				           ||
				   getWatsonPercentage($self->{$a->[0]}) <=> getWatsonPercentage($self->{$b->[0]})
				           ||
				   $a->[0] cmp $b->[0] 
                                  }map {[$_,$self->{$_}->{'count'}]} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_count_ascending { 
   my $self = shift; 
   my @ret = map { $_->[0] } sort {$a->[1] <=> $b->[1]}map {[$_,$self->{$_}->{'count'}]} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_count_descending { 
   my $self = shift; 
   my @ret = map { $_->[0] } sort {$b->[1] <=> $a->[1]}map {[$_,$self->{$_}->{'count'}]} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_gc_ascending { 
   my $self = shift; 
   my @ret = sort { gcp($a) <=>gcp($b) } @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_gc_descending { 
   my $self = shift; 
   my @ret = sort { gcp($b) <=>gcp($a) } @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_length_ascending { 
   my $self = shift; 
   my @ret = sort {length($a) <=> length($b)} @{$self->{'xmers'}};
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_length_descending { 
   my $self = shift; 
   my @ret = sort { length($b) <=> length($a) } @{$self->{'xmers'}}; 
   return wantarray ? @ret : \@ret; 
}
sub xmers_by_percent_watson_ascending { 
   my $self = shift; 
   warn "self:$self\n";
   my @ret = map{$_->[1]}sort{getWatsonPercentage($a->[0]) <=> getWatsonPercentage($b->[0])}map{[$self->{$_},$_]}@{$self->{'xmers'}}; 
   return wantarray ? @ret : \@ret; 
}
sub getWatsonPercentage { 
   my ($watson, $crick) = undef; 
   my $hash = shift; 
   foreach my $seq (keys %{$hash}) { 
      #there is a key 'count', we skip that one and only look at seqs, 
      next if ($seq eq 'count' || $seq !~/^[ATGC]+$/); 
      $watson += $hash->{$seq}->{'watson'}; 
      $crick  += $hash->{$seq}->{'crick'}; 
   }
   return 0 unless $watson; 
   return 100 unless $crick; 
   my $t=$watson+$crick; 
   my $p = 100*($watson/$t);
   return $p; 
}
 
sub gcp {
   my $s = shift; 
   my $t = length $s; 
   if (! $t) { 
     warn "FAILED TO CALC GCP : SEQ sent was $s with length $t\n"; 
     return 0; 
   }
     my $gc = $s=~tr/cCgG/cCgG/; 
   my $gcp = 100 * ($gc/$t); 
   return $gcp; 
}

sub percent_ideal { 
   my $report = shift; 
   my $xmer = shift; 
   my $count = $report->{$xmer}->{'count'};
   my @sequences = grep {$_ !~/count/} keys %{ $report->{$xmer} };
   my $expect = $count/length($xmer);
   ##%ideal = ( $count - (Σ abs($seqCount - $Expect)) ) / $count 
   ##       = (count - WRONG)/count
   my $wrong; 
   foreach my $s (@sequences ) { 
     my $c = $report->{$xmer}->{$s}->{'count'}; 
     $wrong += abs($c-$expect); 
   }
   return 100*(($count-$wrong)/$count);
}

sub sequences { 
    my $report = shift; 
    my $xmer = shift; 
    my @sequences = sort grep { $_ !~/count/} keys %{ $report->{$xmer} }; 
    return wantarray ? @sequences : \@sequences; 
}    

sub counts2percents { 
    my $report = shift;
    my $return = {};  
    # maybe they send us a count (percent reads, etc), or we use the totalcount (percent repeats) 
    #my $count = shift; 
    my $count = $report->sample_total_reads(); 
    $return->{'total_read_count'} = $report->sample_total_reads();   
    my @xmers = xmers_by_count_descending($report); 
    foreach my $xmer (@xmers) { 
         push @{ $return->{'xmers'} }, $xmer unless defined($return->{$xmer}); 
         $return->{$xmer}->{'count'} = 100* ( $report->{$xmer}->{'count'} / $count); 
         $return->{$xmer}->{'crick_count'}  = 100* ($report->{$xmer}->{'crick_count'}/$count); 
         $return->{$xmer}->{'watson_count'} = 100* ($report->{$xmer}->{'watson_count'}/$count); 

         my @sequences = grep {$_ !~/count/} (keys %{ $report->{$xmer} }) ; 
         foreach my $seq (@sequences) { 
            $return->{$xmer}->{$seq}->{'watson'} = 100* ( $report->{$xmer}->{$seq}->{'watson'} / $count ); 
            $return->{$xmer}->{$seq}->{'crick'}  = 100* ( $report->{$xmer}->{$seq}->{'crick'} / $count  ); 
         }
    }
    return bless $return, ref($report); 
}
sub counts2percents_of_repeatunit { 
    #TRTreport, rptunit == @_
    #puts repeat unit counts into percent of all repeat units, and each sequence of the repeat unit is shown as 
    #what percent of the repeatunit count it contributes. 

    my $report = shift;
    my $return = clone($report); 
    my $count  = $return->total_read_count(); 
    my $unit   = shift; 
    foreach my $xmer (xmers_by_count_descending($return)) { 
         push @{ $return->{'xmers'} }, $xmer unless defined($return->{$xmer}); 
         my $unitCount = $return->{$xmer}->{'count'}; 
         $return->{$xmer}->{'count'} = 100* ( $report->{$xmer}->{'count'} / $count); 
         $return->{$xmer}->{'crick_count'}  = 100* ($report->{$xmer}->{'crick_count'}/$count); 
         $return->{$xmer}->{'watson_count'} = 100* ($report->{$xmer}->{'watson_count'}/$count); 

         my @sequences = grep {$_ !~/count/} (keys %{ $report->{$xmer} }) ; 
         foreach my $seq (@sequences) { 
            $return->{$xmer}->{$seq}->{'crick'}  = 100 * ($return->{$xmer}->{$seq}->{'crick'}/$unitCount); 
            $return->{$xmer}->{$seq}->{'watson'}  = 100 * ($return->{$xmer}->{$seq}->{'watson'}/$unitCount); 
         }
    }
    return bless $return, ref($report); 
}

1; #IT TRUE, IT MAGIC, 
