package CDK::SPP::Args;

use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw(
	spp_options
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';


our %options = ('input'		=>"", 
		'experiment' 	=>"",
		'name'		=>"",
		'binding_characteristics_srange'	=>[100,500],
		'binding_characteristics_bin'	  	=>5,
		'cluster'				=>"",
		'smoothed_tag_denisity_bandwidth'	=>200,
		'smoothed_tag_denisity_step'		=>100,
		'conservative_enrichment_profile_step'	=>100,
		'conservative_enrichment_profile_alpha'	=>0.01,
		'include_find_binding_positions'	=>"" ); 

sub spp_options { 
   my %return = %options; 
   return wantarray ? %return : \%return; 
   #$ret{'binding_characteristics_srange'} = [100,500]; 
   #if ( wantarray ) { warn "spp_options wants to return array because you wantarray\n"; } 
#
#   return wantarray ? %ret : \%ret; 
} 

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my %self = %options; 
   return bless \%self, $class; 
}

sub input {
	my $self = shift; 
 	if (@_){ $self->{'input'} = shift; 
	}
	return $self->{'input'}; 
}
    
sub experiment {
	my $self = shift; 
 	if (@_){ $self->{'experiment'} = shift; 
	}
	return $self->{'experiment'}; 
}
sub name  { 
	my $self = shift; 
 	if (@_){ $self->{'name'} = shift; 
	}
	return $self->{'name'}; 
}
sub binding_characteristics_srange {
	my $self = shift; 
 	if (@_){ $self->{'bindingcharacteristics_srange'} = shift; 
	}
	return $self->{'bindingcharacteristics_srange'}; 
}
sub binding_characteristics_bin {
	my $self = shift; 
 	if (@_){ $self->{'binding_characteristics_bin'} = shift; 
	}
	return $self->{'binding_characteristics_bin'}; 
}
sub cluster { 
	my $self = shift;  
 	if (@_){ $self->{'cluster'} = shift; 
	}
	return $self->{'cluster'}; 
}
sub smoothed_tag_density_bandwidth {
	my $self = shift; 
 	if (@_){ $self->{'smoothed_tag_density_bandwidth'} = shift; 
	}
	return $self->{'smoothed_tag_density_bandwidth'}; 
}
sub smoothed_tag_density_step {
	my $self = shift; 
 	if (@_){ $self->{'smoothed_tag_density_step'} = shift; 
	}
	return $self->{'smoothed_tag_density_step'}; 
}
sub conservative_enrichment_profile_step {
	my $self = shift; 
 	if (@_){ $self->{'conservative_enrichment_profile_step'} = shift; 
	}
	return $self->{'conservative_enrichment_profile_step'}; 
}
sub conservative_enrichment_profile_alpha {
	my $self = shift; 
 	if (@_){ $self->{'conservative_enrichment_profile_alpha'} = shift; 
	}
	return $self->{'conservative_enrichment_profile_alpha'}; 
}
sub include_find_binding_positions {
	my $self = shift; 
 	if (@_){ $self->{'include_find_binding_positions'} = shift; 
	}
	return $self->{'include_find_binding_positions'}; 
}
		

1; # THIS is the magical always TRUE ONE that makes modules work. Do not delete it. 
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::SPP::Args - Perl extension for simple management of spp package 

=head1 SYNOPSIS
  traditional usage returns you a hash, which you manipulate
  however you see fit.  Object Oriented usage returns an object 
  that can be used to do the same manipulations in an OO way. 
  spp_options() is the only exportable subroutine in this module. 
  spp_options will return this hash. 

   %options = (	'input'		=>"", 
		'experiment 	=>"",
		'name'		=>"",
		'binding_chararacteristics_srange'	=>[100,500],
		'binding_characteristics_bin'	  	=>5,
		'cluster'				=>"",
		'smoothed_tag_denisity_bandwidth'	=>200,
		'smoothed_tag_denisity_step'		=>100,
		'conservative_enrichment_profile_step'	=>100,
		'conservative_enrichment_profile_alpha	=>0.01,
		'include_find_binding_positions'	=>"" ); 
  
=head1 TRADITIONAL USAGE 
 
  #! /usr/bin/perl 
  use CDK::SPP::Args qw/spp_options/;
  my %spp_options = spp_options(); 
  $spp_options{'input'} 	='/path/file';
  $spp_options{'experiment'} 	='/path/file';
  $spp_options{'name'} 		='output_file_prefix'; 

  
=head1  Object approach
  
  use CDK::SPP::Args; 
  my $args = CDK::SPP::Args->new(); 
  $name = $args->name(); 
  $args->name("10min_HP1_vs10_input"); 
 
  The object approach offers these methods that function to set, or retrieve a property
  $self->input('/path/to/file'); #defaults to undefined value
  $self->experiment('/path/to/file'); #defualt is undefined value
  $self->name('Unt_Input_vs_30m_Input'); 
  $self->binding_characteristics_srange([100,500]); #defaults are [ 100,500 ]
  $self->binding_characteristics_bin(10); #default is 5
  $self->cluster ('TRUE'); #someday this might be functional, currently unimplemented
  $self->smoothed_tag_density_bandwidth(500); #default is 200 
  $self->smoothed_tag_density_step(50); #default is 100 
  $self->conservative_enrichment_profile_step(150); #default is 100
  $self->conservative_enrichment_profle_alpha(0.02); #default is 0.1 

=head1 DESCRIPTION

This is a helper tool for writing wrappers around the SPP R package in perl. 
It provides some reasonable defaults that the CDK::SPP can use when running the package

=head2 EXPORT

None by default.
 spp_option



=head1 SEE ALSO

CDK::SPP

=head1 AUTHOR

cameron kennedy , E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy

=cut
