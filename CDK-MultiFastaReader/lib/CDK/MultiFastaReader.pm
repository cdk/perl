package CDK::MultiFastaReader ; 
use strict;
use subs qw/DEBUG/;
use DBI;
our $VERSION = 1.0;
our $PKG = 'CDK::MultiFastaReader';
our $DEBUG = 0;
our $USE_WHOLE_IDENTIFIER_LINE = undef; 
our $record_start = '>';
sub use_whole_identifier_line { 
	my $class = shift; 
	$USE_WHOLE_IDENTIFIER_LINE = shift; 
}
	sub new {
		my $class = shift; 
		my $fh = shift; 
		my $self = {};
		DEBUG ("setting filehandle to $fh") if $fh;
	 	bless $self, $class; 	
		$self->setFileHandle($fh) if $fh;
		return $self;
	}
	sub setFileHandle {
		my $self = shift;
		$self->{'fh'} = shift;
	}
	sub getFileHandle {
		my $self = shift;
		return $self->{'fh'};
	}
	sub storeID {
		my $self = shift;
		my $id = shift;
		$self->{'id'} = $id;
	}
	sub getStoredID {
	 	my $self = shift;
		return $self->{'id'};
	}
	sub fastForward {
		my $self = shift;
		my $id = shift;
		my $fh = $self->getFileHandle();
		my $line = <$fh>;
		do { $line = <$fh>; warn "$PKG : skipping $line\n" if $line=~/^>/; } until $line=~/$id/; 
		$line=~/ti|(\d+)/; 
		warn "NO ID FOUND IN FastForward : $line\n" unless $1;
        my $set = $1 || "DEFAULTHEADER"; 
		$self->storeID($set); 
		$self->getNextEntry();
	}
	sub getNextEntry {
		my $self = shift;
		my $fh = $self->getFileHandle(); 
		my $id = $self->getStoredID();
		DEBUG "fetching $id\n" if $DEBUG;
		my $seq = undef;
		#if we have $id, we have already been reading the file . . .
		#if not, then this next line better have an id, or something is wrong
		if ($id  eq '') { # this is our first id, i hope, handle as such
			$id = <$fh>;
			chomp($id); 
            warn "using id:$id\n";
			if ( (index($id, $record_start)) >=0 ){
			   ( $id=~/ti\|(\d+)/ || $id=~/>(\S+)/); 
			   
			   die "NO ID FOUND IN STRING $id\n" unless $1 >= 0;
			   DEBUG "my first ID is $1\n" if $DEBUG;
			   $USE_WHOLE_IDENTIFIER_LINE ? $self->storeID($id) : $self->storeID($1) ; 
			   $self->getNextEntry();
			}else {die "what happened here ?"}
		}else {
	 	while (<$fh>){
		   chomp;
		   my $line = $_;
		   #warn "reading $line\n";
		   if ( (index($line, $record_start)>=0)) { 
                        ($line=~/ti\|(\d+)/ || $line=~/>(\S+)/); 
 			$USE_WHOLE_IDENTIFIER_LINE ? $self->storeID($line) : $self->storeID($1);
			#$self->storeID($line);	
			DEBUG "got id $line, returning goodies\n" if $DEBUG;
			DEBUG "got $id before return \n" if $DEBUG;
			DEBUG "seq:$seq\n" if $DEBUG;
			return bless {'-id'=>$id, '-sequence'=>$seq}, "MFSEntry";
		   }else { # we are collecting the sequence for the entry
			#warn "adding $line to seq $seq\n";
			$seq .= $_;
		   }
    		} # done looping through this file
		return undef unless $id && $seq;
		return bless {'-id'=>$id, '-sequence'=>$seq}, "MFSEntry"; 
		
	
	#something wrong if I am here 
	die "Help, I'm lost here!\n";			
        }#end else 
	}
sub DEBUG {
        warn "$PKG :: $_[0]\n" ;
}
1;#it true		
{
package MFSEntry ; 
    sub getID { return $_[0]->{'-id'}}
    sub getSequence {return $_[0]->{'-sequence'}}
	 1;
} #end package MFSEntry

#POD DOCUMENTATION FOLLOWS
=head1 NAME

CDK::MultifastaReader.pm 

=head1 DESCRIPTION

A Perl modules for reading NCBI multifasta sequence files with 'ti' trace identifiers

=head1 SYNOPSIS 

 use CDK::MultiFastaReader;
 use Symbol qw/gensym/;

 my $fh = gensym();
 open($fh, $file) or die $!;

 my $reader = CDK::MultiFastaReader->new();
 $reader->setFileHandle($fh);
 my $fh = $reader->getFileHandle();

 while (my $entry = $reader->getNextEntry() ){
	my $id = $entry->getID();
	my $sequence = $entry->getSequence();
	#do something 
 }


=head1 CLASS METHODS

=over 4

=item  B<new()>


 my $reader =  CDK::MultiFastaReader->new() 
 create a new reader, returns CDK::MultiFastaReader object


=item  B<new($fh)>

 create a new reader and initialize filehandle to $fh
 
 #! /usr/bin/perl 
 use Symbol qw /gensym/;
 my $fh = gensym();
 open($fh, $file) or die $!;
 my $reader = CDK::MultiFastaReader->new($fh);

=back

=head1 OBJECT METHODS 

=over 4

=item  B<getNextEntry()>


 my $entr = $reader->getNextEntry()
 #fetches the next entry from the file
 #do something with entry

 while( my $entry = $reader->getNextEntry() ) {
    #do something with each entry in the file
 } 

=item  B<fastForward($id)>

 Advanced quickly to entry with $id, and then continue reader entries one at a time
 $id='345235453'; #fake number
 $reader->fastForward($id);
 my $entry = $reader->getNextEntry();

=back

=head1 Copyright

 Cameron D Kennedy, 2007
 All rights reserved, no gaurantee, no warranty
=cut
1; #still true ! 


	    
