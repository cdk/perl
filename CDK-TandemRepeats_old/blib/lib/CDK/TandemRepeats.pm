package CDK::TandemRepeats ; 

use strict;
use Symbol qw/gensym/;
use threads; 
use threads::shared; 

our $PKG = 'CDK::TandemRepeats';

sub new {
   my $self = {}; 
   my $class = shift; 
   $class = ref($class) || $class; 
   return bless $self, $class; 
}

sub load {
   my $self = shift; 
   my @files = @_; 
   foreach my $file (@files){
      my $fh = gensym(); 
      open($fh, $file) or die "$PKG:$!\n";
      while(<$fh>){
         chomp; 
         my ($kmer, @seqs) = split "\t", $_; 
         @{ $self }{@seqs}  = $kmer;  
      }
   }
}
         
sub add {
   my $self = shift; 
   my %args = @_; 
   $self->{ $args{'-read'} } = $args{'-unit'}; 
}

sub check {
   my $self = shift; 
   my $want = shift; 
   return $self->{$want} ? $self->{$want} : undef ;
}

sub store { 
   my $self = shift; 
   my $file = shift; 
   my $fh = gensym(); 
   my %output; 
   @output{values %{$self}} = undef; 
   foreach my $k (keys %{$self}){
      push @{ $output{$self->{$k}} }, $k; 
   }
   open ($fh, ">$file") or die "$PKG: $!\n"; 
   foreach my $k (sort keys %output){
      print $fh "$k\t";
      foreach my $seq (@{ $output{$k} }){
         print "\t$seq";
      }
      print "\n";
   }
}
