#!/usr/bin/perl
package BlastParser;
@ISA=('Stone');
use strict;
use Stone;
use Symbol;

sub new {
   my $class=shift;
   my $self=new Stone;
   $self->insert(Files=>@_);
   return bless $self, $class;
}

sub Parse {
   my ($hit,@hits,$blast,@blasts);
   my $self=shift;
   my $sloppy=shift;
   my @files=$self->get('Files');
   foreach my $file (@files){
      $blast=new Stone;
      warn "my file is $file\n";
      my ($program, $version, $query, $query_length, $database)=undef;
      my $fh=Symbol->gensym();
      open($fh,$file) or die "can't open $file:$!";
      my $line=<$fh>;
      ($program,$version)=($1,$2) if $line=~/(\S+)\s+(\S+)/;
      warn "I got program and version\n";
      die "Not a Blast Files from a proper source" unless $program && $version;
      do {$_=<$fh>} until /Query=\s+(\S+)/;
         $query=$1;  
warn "I got a Query\n";
      do {$_=<$fh>} until /\(([0-9,]+)\s+letters.*?\)/;
         $query_length=$1; 
warn "I know the Query Length \n";
      do {$_=<$fh>} until /Database:\s+(\S+)/;
         $database=$1 ;
warn "I got a database\n";
      $blast->insert(Program=>$program,Version=>$version,Query=>$query,
                     Query_length=>$query_length,Database=>$database,
		     File=>$file);
print STDERR "Beginning to process the HITS for $file\n";

warn "we got through the header information\n";	#
      #All of the above is to pull out the 'header' information from
      #a blast file.  I personally never use this information,
      #but have included it for the benefits of my users
      #should i ever have any.
      #Now i will begin the processing of hits
      undef @hits;
      {
         print STDERR "hits undefined . . .\n";
	 
         local $/='>'; # each hit starts with one of these,
	               #experience has shown that some files are
		       #really big, and putting the whole sucker
		       #into memory will slow the process down by 
		       #about 10000
         my $crap=<$fh>;# burn the shit at the front end . . .
	 warn "burned the front end\n";
	 
	  my  $count++;
	 while (<$fh>){
	  
	   print STDERR "count $count  . . . iterating through file $file\n";
	   $hit= _Process_Hit($_);#internal use only
	   push @hits,$hit;
	   $count++;
	 }
      }
      $blast->insert(Hits=>\@hits);
      push @blasts, $blast;
  }#end for loop
  $self->insert(Searches=>\@blasts);
  return $self;
}
  
      
sub _Process_Hit {
warn "inside _Process_Hit subroutine\n";
print STDERR "inside ProcessHit subroutine\n";
     local $/='';
     $_=shift @_; #the hit to be processed
     my $hit=new Stone;
     my $accession = $1 if  /(\S+[^\n]*)/;#take the whole line.
     my $length = $1 if /Length\s=\s(\d+)/;
     $hit->insert(SubjectLength=>$length,Accession=>$accession);
     my @hsps;
#     s/\n/ /gm;
warn "we got accession and length for this hit\n";
     foreach(split /Score\s+=\s+/){
        print "$_\n";
	next if /Length = \d+/;#added this to save time might not work
	my $hsp=new Stone;
        print "created new Stone\n";
        #Score = 845 (132.8 bits)
	my ($score,$bits)=($1,$2) if /(\d+\.?\d*)\s+\((\d+\.?\d*)\s+bits\)/;#was working with /s
        print "got $score and $bits score and bits\n";
	my $sig=$1 if /P(?:\(\d+\))? = (\S+)/;
        print "got sig $sig\n";
	my ($identity,$percent_id)=($1,$2) if /Identities\s+=\s+(\d+\/\d+)\s+\((\d+\%)\)/;
        print "got id $identity and percent_id $percent_id\n";
        my $strand = $1 if m@Strand\s*=\s*([ a-zA-Z/]+)@;#was workign with /s
        print "got $strand\n";
	$strand=~s/NEWLINE//gm;
	if ($strand =~/Minus/){
		$strand = "Minus";
	}else { $strand = "Plus" }

	my $query = $_ if /(Query.*)/;
	my $alignment = $query;#for gary, did it this way
	$query=~s/NEWLINE/\n/g if $query;
	my $query_start=$1 if $query=~/Query: (\d+)/;
	my $subject_start=$1 if $query=~/Sbjct:\s+(\d+)/;
	my $match_length= $1 if $identity=~/(\d+)/;
	$hsp->insert(Score=>$score,Bits=>$bits,Signif=>$sig,
		     Identity=>$identity,Percent_Identity=>$percent_id,
		     Query=>$query,Query_Start=>$query_start,
		     Subject_Start=>$subject_start,Strand=>$strand,
		     MatchLength=>$match_length,
		     Alignment=>$alignment);
	push @hsps,$hsp unless $hsp->Score == '';;
warn "HSPs processed and added to the hit Stone\n";
     }
	$hit->insert(Hsps=>\@hsps);
     return $hit;
}
     
     
     
