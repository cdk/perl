package CDK::FlybaseID;

use strict;
use warnings;
use Carp; 
use Symbol qw/gensym/; 
use File::Temp qw/tempfile tempdir/; 
use PerlIO::Gzip; 
use CDK::Constants qw/FlybaseIDMapURL FlybaseSynonymFile/;
use CDK::Object; 
use Exporter; 
our @EXPORT_OK = qw/shell FB_ID_SHELL/; 
our @EXPORT   	= qw/FB_ID_SHELL/;
our @ISA 	= qw(CDK::Object Exporter);
our $VERSION 	= '0.01';
our $LOAD_FROM_URL =1; 
our $LOAD_FROM_FILE = 0; 
our $PKG	= 'CDK::FlybaseID'; 
my  $DEBUG	= 0; 
sub DEBUG { carp ($PKG, join("\n", @_), "\n")  if $DEBUG} 

sub LoadFromURL 	{ $LOAD_FROM_FILE=undef; $LOAD_FROM_URL=1} 
sub LoadFromFile 	{ $LOAD_FROM_FILE=1; $LOAD_FROM_URL=undef; }  

sub new { 	
	DEBUG ("new");
	my $that = shift; 
	my $class = ref($that) || $that; 
	my $self = $class->SUPER::new(); 
	bless $self, $class; 
	$self->_download_and_parse() if $LOAD_FROM_URL;
   $self->_load_and_parse() if $LOAD_FROM_FILE;  
	croak "$PKG: bad luck downloading the flybase id data, check your sources\n"
		unless $self->properties() > 1; 
	return $self; 
}

sub FB_ID_SHELL { shell(@_) } 
sub shell { 
	DEBUG("shell");
	my $fb = undef; 
	if (@_) { 
	 $fb = shift; 
	}else { $fb = CDK::FlybaseID->new() } 

	print "Enter a flybase identifier [q to quit] : "; 
while( <> ){
	chomp; 
	my $want = $_; 
		  DEBUG("you entered $want"); 
		  chomp $want; 
		  exit if $want eq 'q'; 
		  DEBUG("$want is not q, continue");  
        if ( $fb->get($want) > 0 ){
					DEBUG("More than zero fb_ids found in $fb\n");
					my $id_info = $fb->get($want); 
					DEBUG("FB_ID info : $id_info"); 
                print "fb_id : ", $id_info->FB_ID(), "\n" ;
                print "symbol: ", $id_info->Symbol(), "\n";  
                print "name : ", $id_info->Name(), "\n";
					 my @name_syns = $id_info->Name_Synonyms(); 
					 my @symb_syns = $id_info->Symbol_Synonyms(); 
                print "name_synonyms : ", join (",", @name_syns), "\n";
                print "symbol_synonyms : ", join (",", @symb_syns), "\n";
        }
        else {
                print "I don't know flybase id : $_\n";
        }
        print "\nEnter a flybase identifier [q to quit] : ";
}

}

sub _load_and_parse { 
	my $self = shift; 
	my $file = FlybaseSynonymFile(); 
	my $fh = gensym(); 
	open($fh, "<:gzip",$file) or croak "$!\n"; 
   $self->_parse($fh); 
}	


sub _download_and_parse { 
	require LWP::Simple; 
	  
	my $self = shift; 
	my ($fh, $file) = tempfile(); 
	close $fh; 
	my $url = FlybaseIDMapURL; 
	DEBUG("fetching $url to $file\n"); 
 	my $rc = LWP::Simple::getstore($url, $file); 
	DEBUG("returned code : $rc"); 
 	
	open($fh, "<:gzip", $file) or croak "$!";
	DEBUG("opened file for reading, now parsing into objects\n");
	$self->_parse($fh); 
	unlink $file; 	
}
sub _parse { 
   no warnings qw/uninitialized/; 
	my $self = shift; 
	my $fh = shift; 
	while(<$fh>){
		chomp;
		my $line = $_; 
		my ($fb_id, $symbol, $full_name, $name_synonyms, $symbol_synonyms) = map{ $_ ? $_ : "noValue"} split "\t", $line;
		
		my @name_synonyms = split ",", $name_synonyms;
		my @symbol_synonyms = split ",", $symbol_synonyms;
		my $id_info = CDK::Object->new( 	'FB_ID'				=>	$fb_id, 
												  	'Symbol'				=> $symbol,
													'Name'				=> $full_name, 
													'Name_Synonyms'	=>	\@name_synonyms, 
													'Symbol_Synonyms'	=> \@symbol_synonyms ); 
		$self->set($fb_id, $id_info); 
		$self->set($symbol, $id_info); 
		foreach (@name_synonyms, @symbol_synonyms) { 
			$self->set($_, $id_info); 
		}
	}	
   close $fh;
   DEBUG("data loaded"); 
}

sub check { 
	my $self = shift; 
	my $id = shift; 
   return $self->hasProp($id); 
}
sub identifiers { 
    my $self = shift; 
    return $self->properties();
}
sub flybase_id { 
	my $self = shift; 
	my $id 	= shift; 
	return $self->get($id)->FB_ID;
}
sub symbol { 	
	my $self = shift; 
	my $id 	= shift; 
	return $self->get($id)->Symbol; 
}
sub name	{
	my $self	= shift; 
	my $id 	= shift; 
	return wantarray ? @{ $self->get($id)->Name } : $self->get($id)->Name ;  
}
sub name_synonyms { 
	my $self = shift; 
	my $id 	= shift; 	
	my $syns = $self->get($id)->Name_Synonyms; 
	return wantarray ? @{ $syns } : $syns; 
} 
sub symbol_synonyms { 
	my $self = shift; 
	my $id 	= shift; 
	my $syns = $self->get($id)->Symbol_Synonyms; 
   return wantarray ? @{ $syns } : $syns; 
} 




1;# IT TRUE ! ALWAYS END MODULE TRUE ! 
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::FlybaseID - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::FlybaseID;
  my $fb = CDK::FlybaseID->new(); 
  my $want = "CENPA";    
  if (  $fb->get($want) > 0 ){
        my $info = $fb->get($want); 
        my $flybaseID = $info->FB_ID; 
        my $symbol    = $info->Symbol; 
        my $name      = $info->Name; 
        my @name_synonyms = $info->Name_Synonyms; 
        my @symbol_synonyms = $info->Symbol_Synonyms; 
        print "FB_ID:$flybaseID\n"; 
        print "symbol:$symbol\n";
        print "name: $name\n"; 
        print "name_synonyms : ", join ",", @name_synonyms; 
        print "\n"; 
        print "symbol_synonyms : ", join ",", @symbol_synonyms; 
 } else { 
        print "You wack.  Something wrong\n"; 
}
        
=head1 DESCRIPTION

CDK::FlybaseID : an object for accessing flybase id information via objects and methods

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
