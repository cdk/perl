package CDK::ThreadableObject ; 

use strict; 
use threads; 
use threads::shared qw/share is_shared/;
use Scalar::Util qw/reftype blessed/; 
   
our $VERSION = 0.01; 
our $PKG = 'CDK::ThreadableObject'; 
   
sub new { 
   my $class = shift; 
   share(my %self);
   while (my $prop = shift @_){
      if (my $val = shift){
         $self{$prop}  = _make_shared($val);
      }else {
         warn "$PKG: I think you forgot to send in a value for $prop\n";
      }
   }
   return bless (\%self, $class);
}

sub pushOn { 
   my $self = shift; 
   my $prop = shift; 
   if (ref($self->{$prop}) eq 'ARRAY'){
      if (my $val = shift){ 
         warn "$PKG: adding $val to $self->{$prop}\n";
         push @{ $self->{$prop} }, _make_shared($val);
      }else { 
         warn "$PKG: no value to add to $self->{$prop}\n";
      }
   }else {
      warn "$PKG: sub pushOn expect array reference, not $self->{$prop}, " . ref($self->{$prop}). " okay ? \n"; 
   }
}
sub set { 
   my $self = shift;  
   if ( my $prop = shift){ 
      if (my $val = shift){
         warn "$PKG: setting prop $prop to val $val\n"; 
         if (ref($val) eq 'ARRAY'){
            warn "$PKG: val : " . @$val. "\n";
         }elsif (ref($val) eq 'HASH'){
            warn "$PKG: val : ". %{ $val } . " \n"; 
         }
         lock $self;
         $self->{$prop}=_make_shared($val);
      }else { 
         warn "$PKG: setting $prop to undef\n";
         lock $self; 
         $self->{$prop} = undef; 
      }
   }else { 
      warn "$PKG: no idea why set was called with prop \= \'$prop\'\n";
      my ($package, $filename, $line) = caller;
      warn "$PKG: package:$package, file:$filename, line:$line\n";
   }
}

sub hasProp{ 
   my $self = shift; 
   my $prop = shift; 
   return exists $self->{$prop}
}

sub _make_shared {
   my $test = shift; 
   #if shared, return
   return $test if is_shared($test);
   #otherwise, we return this
   my $return = undef;
   #normally, we receive reference of the following types
   if (my $ref_t = reftype($test)){
      if ($ref_t eq 'ARRAY'){
         $return = &share([]); 
         foreach my $item (@{$test}){
            push @{$return}, _make_shared($item); 
         }
       }elsif( $ref_t = 'HASH'){
          $return = &share({}); 
          foreach my $k (%{$test}){
             $return->{$k} = _make_shared($test->{$k});
          }
       }elsif ($ref_t = 'SCALAR'){ 
          $return = \do{ my $scalar = $$test; }; 
       }
    
       if ($return ) { 
           #check if has readonly flag set, and handle
          if (Internals::SvREADONLY($test)) { 
              Internals::SvREADONLY($return, 1);
          }
          #if the thing is blessed, then bless the return
          if ( my $class = blessed($test) ) { 
             bless($return, $class);
          }
          #give it back
          return $return; 
       }
    }
    #hope this is just a simple scalar value, 
    #since we have no reference, we just return it
    return $test ;
}

1; # Its true ! 