package CDK::ThreadSafe::LibraryManager;

use threads; 
use threads::shared; 
use strict;
use Symbol qw/gensym/; 
require CDK::ThreadableObject; 
use Thread::Queue; 
our @ISA = qw/CDK::ThreadableObject/; 
our $PKG = 'CDK::ThreadSafe::LibraryManager'; 
our $DEBUG = 1; 

sub new { 
   my $class = shift; 
   my $self = $class->SUPER::new(); 
   return $self ;
}

sub setLibraryDefinition ($){ 
   my $self = shift; 
   my $file = shift; 
   my $name = undef; 
   my $FH = gensym();
   my $first = CDK::ThreadableObject->new();
   $self->set('-first'=>$first); 
   open($FH, "$file") or die "$PKG:$!:can't open file\n"; 
   #name	FilePath
   my @libs = <$FH>; 
   my $current = $first;
   for(my $i=0; $i<$#libs; $i++) { 
         my($name, $file) = split "\t", $libs[$i]; 
         $current->set('-name'=>$name); 
         $current->set('-file'=>$file); 
         $current->set('-queue'=>Thread::Queue->new() ); 
         $current->set('-next'=>CDK::ThreadableObject->new()); 
         $current = $current->{'-next'};
    }
    ($name, $file) = split "\t", $libs[$#libs]; 
    $current->set('-name'=>$name); 
    $current->set('-file'=>$file); 
    $current->set('-next'=>undef);
 }
 
 1; #it true; it magic; it real;  
 
