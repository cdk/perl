package CDK::Mask;
use strict; 
use Set::IntSpan::Fast; 

our $DEBUG=1; 

sub new { 
   my $that = shift; 
   my $class = ref($that) || $that; 
   my $self = {}; 
   $self->{'-mask'} = Set::IntSpan::Fast->new(); 
   return bless $self, $class; 
}
sub add { 
   my $self = shift; 
   my $mask = $self->{'-mask'}; 
   foreach (@_) { 
     if (/(\d+)-(\d+)/) { 
       $mask->add_range($1, $2); 
     }else { 
       $mask->add($_); 
     } 
   }
}

sub check {
   my $self = shift; 
   my $check = shift; 
   my $mask = $self->{'-mask'}; 
   my $in = $mask->contains($check); 
   warn "mask contains : $in <-\$in\n" if $DEBUG; 
   return $in; 
}

1; #IT TRUE, IT ALWAYS TRUE, IT MAGIC TOO 
