package CDK::TandemRepeats ; 

use strict;
use Symbol qw/gensym/;
use threads; 
use threads::shared; 

our $PKG = 'CDK::TandemRepeats';
our $VERSION = 0.01; 

sub new {
   my $self = {}; 
   my $class = shift; 
   $class = ref($class) || $class; 
   return bless $self, $class; 
}

sub load {
   my $self = shift; 
   warn "$PKG: load : @_\n"; 
   my @files = ref($_[0] eq 'ARRAY') ? @{ +shift } : @_; 
   foreach my $file (@files){
      warn "$PKG : file -- $file\n"; 
      my $fh = gensym(); 
      open($fh, $file) or die "$PKG:$!\n";
      while(<$fh>){
         chomp; 
         my ($kmer, @seqs) = split "\t", $_; 
         foreach (@seqs){
              $self->{$_} = $kmer; 
         }
         #@{ $self }{@seqs}  = $kmer;  
      }
   }
   return 1; 
}
         
sub add {
   my $self = shift; 
   my %args = @_; 
   $self->{ $args{'-read'} } = $args{'-unit'}; 
}

sub check {
   my $self = shift; 
   my $want = shift; 
   return $self->{$want} ;
}

sub store { 
   my $self = shift; 
   my $file = shift; 
   my $fh = gensym(); 
   my %output; 
   @output{values %{$self}} = undef; 
   foreach my $k (keys %{$self}){
      push @{ $output{$self->{$k}} }, $k; 
   }
   open ($fh, ">$file") or die "$PKG: $!\n"; 
   foreach my $k (sort keys %output){
      print $fh "$k\t";
      foreach my $seq (@{ $output{$k} }){
         print $fh "\t$seq";
      }
      print $fh "\n";
   }
}
sub count {
   my $self = shift; 
   return scalar keys %{$self};
}


1; # it true ! 
