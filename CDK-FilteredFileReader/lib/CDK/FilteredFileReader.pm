package CDK::FilteredFileReader;

use strict;
use warnings;
use Symbol qw/ gensym /; 
use CDK::List; 
use CDK::Iterator; 

our $VERSION = '0.01';
our $PKG     = 'CDK::FilteredFileReader'; 

#so basically, we take a string that we interpret as a file path, 
#we accept a subroutine reference as a filter. 
#the subroutine should return 1 if accepted or 0 if rejected. 

sub new { 
   my $that = shift; 
   my $class = ref($that)||$that; 
   my $self = {}; 
   return bless $self, $class; 
}

sub file { 
   my $self = shift; 
   if (@_){ 
	$self->{'_file'} = shift @_; 
	$self->_openFile(); 
   }   
   return $self->{'_file'}; 
}

sub filterList { 
   my $self = shift; 
   my $filterList = shift; 
   if ($filterList){ 
      	$self->{'_filterList'} = $filterList; 
	$self->{'_filterIterator'} = $self->{'_filterList'}->getIterator(); 
   }else { 
   return $self->{'_filterList'}; 
   }
} 

sub fileHandle { 
   my $self = shift; 
   $self->{'_fh'} = shift if @_; 
   return $self->{'_fh'}; 
}

sub filter { 
   my $self = shift; 
   if (@_ ) { 
      $self->{'_filter'} = shift;
      $self->{'_filterIterator'} ||= CDK::Iterator->new([]) ;
   }
   return $self->{'_filter'}; 
}

sub hasMoreFilters {
   my $self = shift; 
   return $self->{'_filterIterator'}->hasNext(); 
} 

sub nextFilter { 
   my $self = shift; 
   $self->_openFile(); 
   $self->filter($self->{'_filterIterator'}->next()); 
   $self->_openFile();
   return $self->filter(); 
}

sub readLine { 
   my $self = shift; 
   my @ret; 
   my $fh = $self->fileHandle(); 
   my $filter = $self->filter(); 
   if (ref($filter) eq 'CODE' || ref($filter) eq 'CDK::TextFilter'){ 
      while(<$fh>){
	chomp; 
	@ret = $filter->($_);
        my $good = grep { $_ ? 1 : 0} @ret; 
          if ($good) { 
		return wantarray ? @ret : \@ret;
          }
       }
       return 'EOF'; 
   }elsif (ref($filter) eq 'Regexp'){
      while(<$fh>){chomp; return $_ if /$filter/}
   }
   return undef; 
}

sub readAllLines { 
   my $self = shift; 
   my $fh = $self->fileHandle(); 
   my $filter = $self->filter(); 
   my @return; 
   if (ref($filter) eq 'CODE'){
	while(<$fh>){
		chomp; 
		my $ret = $filter->($_); 
		push(@return, $ret) if $ret; 
	}
    }elsif (ref($filter) eq 'Regexp'){
	while(<$fh>){
		chomp; 
		push @return, $_ if /$filter/; 
	}
    }else { 
	warn "$PKG: no idea what to do with $filter" . ref($filter) . ", sorry";
    }
    return wantarray ? @return : \@return; 
}	
 
sub _openFile { 
   my $self = shift; 
   my $file = $self->file(); 
   
   my $fh = $self->fileHandle(); 
   close $fh if $fh;  
   open ($fh, $file) or die "failed to open critical file:$file:$!\n"; 
   $self->fileHandle($fh); 
} 



1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::FilteredFileReader - Perl extension for reading selected lines from a file

=head1 SYNOPSIS

  use CDK::FilteredFileReader;
  my $reader = CDK::FilteredFileReader->new(); 
  my $filter = CDK::FilterFactory::createFilter("match this"); 
  $reader->filter($filter); 
  $reader->file("filePath/file"); 
  while(my $line = $reader->readLine()){ 
      chomp $line 
	...
  } 

  #mutiple filters 
  my $filterList = CDK::FilterFactory::createFilterList($CDK::List); 
  $reader->filterList($filterList); 
  while($reader->hasMoreFilters) { 
    $reader->nextFilter(); 
	while(my $line = $reader->readLine()){
		chomp $line; 
		...
   	}
   }

  #read all the matching lines 
  my @Lines = $reader->readAllLines(); 
  #or get a reference
  my $lines = $reader->readAllLines(); 


=head1 DESCRIPTION

This tool reads a file, and uses a supplied filter or list of filters to retrieve lines matching that pass the filter. 
This tool is closely related to, or depends upon, CDK::Iterator, CDK::List, CDK::FilterFactory, CDK::ListFilter, and 

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
