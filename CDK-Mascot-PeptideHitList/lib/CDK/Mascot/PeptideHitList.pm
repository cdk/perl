package CDK::Mascot::PeptideHitList;

use strict;
use warnings;
use CDK::List; 
our @ISA = qw(CDK::List);
our $VERSION = '0.01';
our $PKG = 'CDK::Mascot::PeptideHitList'; 

our $DEBUG = 0; 
sub DEBUG { warn "$PKG:@_\n" if $DEBUG } 
sub DEBUG_ON { $DEBUG = 1} 
sub DEBUG_OFF { $DEBUG=undef } 

sub add { 
   my $self = shift; 
   my $peptideHit = shift; 
   if ($peptideHit->isa("CDK::Mascot::PeptideHit") ) { 
	$self->SUPER::add($peptideHit); 
   }else { 
	warn "$PKG: was expecting CDK::PeptideHit not $peptideHit in CDK::Mascot::PeptideHitList::add\n";
   }
}


1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Mascot::PeptideHitList - Module for collecting peptideHit objects in a CDK::list

=head1 SYNOPSIS

  use CDK::Mascot::PeptideHitList;
  my $ph = new CDK::Mascot::PeptideHit(); 
  my $phl = CDK::Mascot::PeptideHitList->new(); 
  $phl->add($ph); 
  #get peptides in the list as array 
  my @peptides = $phl->getArray(); 
 
  #use iterator to access the peptides in the hit
  for($itr = $phl->getIterator(); $itr->hasNext(); ) { 
	$peptide = $itr->next(); 
  } 

  
=head1 DESCRIPTION

Minimal List object for storing a list of peptides. 

=head2 EXPORT

NONE BY DEFAULT

=head1 SEE ALSO

CDK::List , CDK::Mascot::PeptideHit, CDK::Mascot::ProteinHit CDK::Mascot::ProteinHitList

=head1 AUTHOR

cameron, E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
