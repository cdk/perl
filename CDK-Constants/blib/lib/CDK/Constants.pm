package CDK::Constants;

use strict;
use warnings;

require Exporter;
our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use CDK::Constants ':all';
our %EXPORT_TAGS = ( 'all' => [ qw(CodonUsageTable FlybaseIDMapURL FlybaseSynonymFile ChopAndSpiceURL ChopAndSpiceXMLTemplate TandemRepeats SizeOfMap SizeOfMapDIR) ] ); 

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( ) ;
	
our $VERSION = '0.01';
our $SIZEOFMAP=undef;
sub CodonUsageTable { return '/data/codon_tables/drosophila_melanogaster/cdk_codon_table'} 
 
sub FlybaseIDMapURL { return 'http://flybase.org/static_pages/downloads/FB2010_06/synonyms/fb_synonym_fb_2010_06.tsv.gz' } 

sub FlybaseSynonymFile { return '/Volumes/LabData/SHARED_DATA/FLYBASE_PRECOMPUTED_FILES/flybase_synonyms_2010_06.tsv.gz'}

sub ChopAndSpiceURL { return 'http://chopnspice.gwdg.de/chopnspice/webservice.php' } 

sub ChopAndSpiceXMLTemplate { return '/Volumes/LabData/SHARED_DATA/Chop_and_Spice_XML_Template.xml' } 

sub TandemRepeats { return '/Volumes/LabData/cameron/data/tandem_repeat_subunits'}

sub SizeOfMapDIR() {return '/Volumes/LabData/SHARED_DATA/SizeOfMaps/'}

sub SizeOfMap { 
   require Symbol;
   import Symbol qw/gensym/;
   if ( $SIZEOFMAP  ){ return $SIZEOFMAP}
   my $map_dir = SizeOfMapDIR(); # 
   my @files = <$map_dir/*.tab>;
   foreach my $f (@files){
      my $fh = gensym();
      open($fh, $f) or die "$!";
      while(<$fh>){
         chomp;
         my($k,$v)=split "\t";
         $SIZEOFMAP->{$k}=$v;
      }
   }
   return $SIZEOFMAP; 
}


1; # TRUE !!!  MAGIC !!!
__END__

=head1 NAME

CDK::Constants - Things I don't want to remember

=head1 SYNOPSIS

  use CDK::Constants qw/all/;
  use LWP::Simple; 
  use PerlIO::Gzip; 
  
  my $codon_usage = CodonUsageTable;
  my $flybaseIDMapURL = FlybaseIDMapURL; 
  
  getstore($flybaseIDMapURL, '/path/to/file.tsv.gz'); 
  open(FBID, "<:gzip", 'path/to/file.tsv.gz') or die "MAIN : $!";
  #do something with the data in the file, read in from FBID 
  

=head1 DESCRIPTION

Sub routines that return strings I might otherwise forget that I need. 

	&CodonUsagetable returns the path to the codon 
	usage table. it needs edited with installation 
	on each machine.
	
	&FlyBaseIDMapURL returns the string for the URL
	where I last found the map of Flybase identifiers to other known
	names, and symbols. 

	&FlybaseSynonymFile returns the path to the synonym file
	
	&ChopAndSpiceURL returns URL for chop and spice web service 
   
   &SizeOfMap returns hashref of id to size in bp mappings
   
   &SizeOfMapDIR returns a string for the path to the directory where the size of maps are stored
=head2 EXPORT

None by default.


=head1 SEE ALSO

=head1 AUTHOR

cameron kennedy, E<lt>cameron@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007 by cameron kennedy <cameron.kennedy@gmail.com>

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
