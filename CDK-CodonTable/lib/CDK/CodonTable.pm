package CDK::CodonTable;

use strict;
use warnings;
use Symbol qw/gensym/;
our $VERSION = '0.01';

sub new { 
   my $class = shift; 
   my $self = {'CODON'=>{}, 'AA'=>{} };  
   return bless $self, $class; 
}
sub setCodonFile{ 
   my $self = shift; 
   my $table = shift;
   my $fh = gensym(); 
   open($fh, $table) or die "$!"; 
   while(<$fh>){ #codon	AA	Fraction
      next unless /^([ATGC]{3})\s+([A-Z])\s+([0-9.]+)/;
      warn "codon: $1\tAA: $2\tfract: $3\n"; 
      $self->{'CODON'}->{$1} = $2; 
      push @ { $self->{'AA'}->{$2} }, {'codon'=>$1, 'fract'=>$3} ;
   }
}

sub getAA { 
   my $self=shift; 
   my $codon =  shift; 
   print "you passed me $codon\n"; 
   return $self->{'CODON'}->{$codon} or warn "no AA for codon $codon\n"; 
}
sub getCodons { 
   my $self = shift; 
   my $aa = shift; 
   print "you passed me aa:$aa\n"; 
   return $self->{'AA'}->{$aa}; 
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::CodonTable - Crappy codon data interface for me

=head1 SYNOPSIS

  use CDK::CodonTable;
  blah blah blah

=head1 DESCRIPTION

the author of the extension was negligent enough to leave the stub
unedited.


=head1 AUTHOR

cameron kennedy, E<lt>cameron@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007 by cameron kennedy

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
