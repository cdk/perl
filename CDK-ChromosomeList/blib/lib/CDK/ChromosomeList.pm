package CDK::ChromosomeList;

use strict;
use warnings;
use CDK::List; 
require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# This allows declaration	use CDK::ChromosomeList ':all';
our %EXPORT_TAGS = ( 'all' => [ qw( 
	dmel_all_list
	dmel_eland_list
	dmel_flybase_list
	dmel_igby_list	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION = '0.01';

our @ELAND_EXPORT_NAMES = qw/ 	arm2L
				arm2R
				arm3L
				arm3R
				arm4
				armX
				armU
				armUextra
			 /; 

our @IGBY_BROWSER_NAMES = qw/	chr2L
				chr2LHet
				chr2R
				chr2RHet
				chr3L
				chr3LHet
				chr3R
				chr3RHet
				chr4
				chrU
				chrX
				chrXHet
				chrYHet
	    		 /; 

our @FLYBASE_RELEASE_NAMES = qw/2L
				X
				3L
				4
				2R
				3R
				Uextra
				2RHet
				2LHet
				3LHet
				3RHet
				U
				XHet
				YHet
			 	dmel_mitochondrion_genome
				/; 

sub dmel_all_list 	{ return CDK::List->new(@IGBY_BROWSER_NAMES, @FLYBASE_RELEASE_NAMES, @ELAND_EXPORT_NAMES) ; } 
sub dmel_eland_list 	{ return CDK::List->new(@ELAND_EXPORT_NAMES); } 
sub dmel_flybase_list 	{ return CDK::List->new(@FLYBASE_RELEASE_NAMES); } 
sub dmel_igby_list 	{ return CDK::List->new(@IGBY_BROWSER_NAMES); } 
 
1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::ChromosomeList - Perl extension for retrieving a list of chromosomes

=head1 SYNOPSIS

  use CDK::ChromosomeList qw/	dmel_all_list
				dmel_eland_list
				dmel_flybase_list
				dmel_igby_list
			   /;

  #this returns a CDK::List object, see CDK::List for details
  $igby_xsomes = dmel_igby_list(); 
   
=head1 DESCRIPTION

This is just a way to get a LIST of chromosomes in various formats used in various file formats 

=head2 EXPORT

None by default.
dmel_all_list
dmel_eland_list
dmel_flybase_list
dmel_igby_list
=head1 SEE ALSO

CDK::List, CDK::FilterFactory

=head1 AUTHOR

cameron kennedy , E<lt>cameron.kennedy@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
