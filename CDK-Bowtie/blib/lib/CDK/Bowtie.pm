package CDK::Bowtie;

use 5.010000;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);
our $PKG = 'CDK::Bowtie';
#set the bowtie index environment variable for ourselves and our children
our %EXPORT_TAGS = ( 'all' => [ qw(
    run_bowtie_on_euch
    run_bowtie_on_repeatmasked_euch
    run_bowtie_on_all_het
    run_bowtie_on_all_repeatmasked_het
    run_bowtie_on_het_extensions
    run_bowite_on_four_and_Y
    run_bowtie_on_mito
    run_bowtie_on_transposon
    run_bowtie_on_known_repeats
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';
$ENV{'BOWTIE_INDEXES'} ||= '/Volumes/CK_GoDrive/SimpleRepeats/bowtie-indexes/'; 
our $EUCH           = 'dmel-5.32-euch';
our $REPEATMASKED_EUCH           = 'dmel_r5_euchromatin_rptmsk';
our $HET            = 'dmel-5.32-allHet-noU'; 
our $REPEATMASKED_HET            = 'dmel_r5_all_heterochromatin_rptmsk'; 
our $HET_EXT        = 'dmel-5.32-hetEXT';
our $Four_AND_Y     = 'dmel-5.32-4andY'; 
our $MITO           = 'dmel-mito';
our $TE             = 'dmel-all-transposon-r5.20'; 
our $KNOWN_REPEATS  = 'drosophila_known_repeats'; 
our $CDK_UNIQUE     = 'cams_uniq_genome';
our $shellIORedirection = '3>&1 1>&2 2>&3 3>&-'; 
sub DEBUG { warn "$PKG: @_\n"} ; 
sub run_bowtie_on_euch ($) { 
    my $fastq       = shift; 
    my $alignedF     = "${fastq}.euchAligned"; 
    my $unalignedF   = "${fastq}.euchUnaligned"; 
    my $maxAlignedF  = "${fastq}.euchMaxAligned"; 
    my $bowtie_resultF = "${fastq}.euchBowtie"; 
    #bowtie -q -m 3 -al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $EUCH $fastq $bowtie_resultF"; 
    my      @r = `$command $shellIORedirection`; 
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r; 
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r; 
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r; 
    my ($multiAligned)= map /due to -m: (\d+)/, grep /suppressed due to /, @r; 
    DEBUG("\@r=@r\nread_count\t$read_count\n\taligned\t$aligned\n\tunaligned\t$unaligned\n\tmultiAligned\t$multiAligned\n"); 
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF); 
}

sub run_bowtie_on_repeatmasked_euch ($) { 
    my $fastq       = shift; 
    my $alignedF     = "${fastq}.euchAligned"; 
    my $unalignedF   = "${fastq}.euchUnaligned"; 
    my $maxAlignedF  = "${fastq}.euchMaxAligned"; 
    my $bowtie_resultF = "${fastq}.euchBowtie"; 
    #bowtie -q -m 3 -al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $REPEATMASKED_EUCH $fastq $bowtie_resultF"; 
    my      @r = `$command $shellIORedirection`; 
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r; 
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r; 
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r; 
    my ($multiAligned)= map /due to -m: (\d+)/, grep /suppressed due to /, @r; 
    DEBUG("\@r=@r\nread_count\t$read_count\n\taligned\t$aligned\n\tunaligned\t$unaligned\n\tmultiAligned\t$multiAligned\n"); 
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF); 
} 

sub run_bowtie_on_all_het ($) { 
    my $fastq       = shift; 
    my $alignedF     = "${fastq}.hetAligned"; 
    my $unalignedF   = "${fastq}.hetUnaligned"; 
    my $maxAlignedF  = "${fastq}.hetMaxAligned"; 
    my $bowtie_resultF = "${fastq}.hetBowtie"; 
    #bowtie -q -m 3 --al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $HET $fastq $bowtie_resultF"; 
    my      @r = `$command $shellIORedirection`; 
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r; 
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r; 
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r; 
    my ($multiAligned) = map /due to -m: (\d+)/, grep /suppressed due to /, @r; 
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF); 
}

sub run_bowtie_on_all_repeatmasked_het ($) { 
    my $fastq       = shift; 
    my $alignedF     = "${fastq}.hetAligned"; 
    my $unalignedF   = "${fastq}.hetUnaligned"; 
    my $maxAlignedF  = "${fastq}.hetMaxAligned"; 
    my $bowtie_resultF = "${fastq}.hetBowtie"; 
    #bowtie -q -m 3 --al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $REPEATMASKED_HET $fastq $bowtie_resultF"; 
    my      @r = `$command $shellIORedirection`; 
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r; 
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r; 
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r; 
    my ($multiAligned) = map /due to -m: (\d+)/, grep /suppressed due to /, @r; 
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF); 
}

sub run_bowtie_on_mito ($) {
    my $fastq       = shift;
    my $alignedF     = "${fastq}.mitoAligned";    my $unalignedF   = "${fastq}.mitoUnaligned";
    my $maxAlignedF  = "${fastq}.mitoMaxAligned";    my $bowtie_resultF = "${fastq}.mitoBowtie";
    #bowtie -q -m 3 -al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $MITO $fastq $bowtie_resultF";
    my      @r = `$command $shellIORedirection`;
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r;
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r;
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r;
    my ($multiAligned)= map /due to -m: (\d+)/, grep /suppressed due to /, @r;
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF);
}

sub run_bowtie_on_transposon ($) {
    my $fastq       = shift;
    my $alignedF     = "${fastq}.transposonAligned";    my $unalignedF   = "${fastq}.transposonUnaligned";
    my $maxAlignedF  = "${fastq}.transposonMaxAligned";    my $bowtie_resultF = "${fastq}.transposonBowtie";
    #bowtie -q -m 3 -al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $TE $fastq $bowtie_resultF";
    my      @r = `$command $shellIORedirection`;
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r;
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r;
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r;
    my ($multiAligned)= map /due to -m: (\d+)/, grep /suppressed due to /, @r;
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF);
}

sub run_bowtie_on_known_repeats ($) {
    my $fastq       = shift;
    my $alignedF     = "${fastq}.knownrepeatsAligned";    my $unalignedF   = "${fastq}.knownrepeatsUnaligned";
    my $maxAlignedF  = "${fastq}.knownrepeatsMaxAligned";    my $bowtie_resultF = "${fastq}.knownrepeatsBowtie";
    #bowtie -q -m 3 -al ${i}.euchAligned --un ${i}.euchUnaligned --max ${i}.multipleAlign -p 2 --mm dmel-5.32-euch ${i} ${i}.bowtie
    my $command = "bowtie -q -m 3 --al $alignedF --un $unalignedF --max $maxAlignedF -p 2 --mm $KNOWN_REPEATS $fastq $bowtie_resultF";
    my      @r = `$command $shellIORedirection`;
    #@r=    # reads processed: 62177372
            # reads with at least one reported alignment: 48313376 (77.70%)
            # reads that failed to align: 11704838 (18.82%)
            # reads with alignments suppressed due to -m: 2159158 (3.47%)
            #Reported 48313376 alignments to 1 output stream(s)
    my ($read_count)  = map /processed: (\d+)/, grep /reads processed/, @r;
    my ($aligned)     = map /alignment: (\d+)/, grep /reported alignment/, @r;
    my ($unaligned)   = map /failed to align: (\d+)/, grep /failed to align/, @r;
    my ($multiAligned)= map /due to -m: (\d+)/, grep /suppressed due to /, @r;
    return ($aligned, $multiAligned, $unaligned, $bowtie_resultF, $alignedF, $unalignedF, $maxAlignedF);
}


1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::Bowtie - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::Bowtie;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::Bowtie, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
