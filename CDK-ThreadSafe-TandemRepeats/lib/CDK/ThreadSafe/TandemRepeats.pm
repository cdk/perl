package CDK::ThreadSafe::TandemRepeats;

use strict;
use Symbol qw/gensym/;
use threads; 
use threads::shared; 
use CDK::ThreadableObject; 
use Fcntl ':flock'; # import LOCK_* constants


our $PKG = 'CDK::ThreadSafe::TandemRepeats';
our $VERSION = 0.01; 
our @ISA = q/CDK::ThreadableObject/;

sub new {
   my $class = shift; 
   my $self = $class->SUPER::new(@_); 
   return $self; 
}

sub load {
   my $self = shift; 
   my @files = @{ +shift }; 
   $self->set("loaded", \@files);
   foreach my $file (@files){
      warn "$PKG : file -- $file\n"; 
      my $fh = gensym(); 
      open($fh, $file) or die "$PKG:$!\n";
      flock($fh, LOCK_EX) or warn "$PKG: no lock in flock call\n"; 
      while(<$fh>){
         chomp; 
         my ($kmer, @seqs) = split "\t", $_;        
         next unless ($kmer && (scalar @seqs >0));
         foreach my $seq (@seqs){
              warn "$PKG:  sub load, kmer:$kmer\tseq:$seq\n";
              $self->set($seq, $kmer); 
              #$self->{$_} = &share($kmer); 
         }   
         
      }
      flock($fh, LOCK_UN) or warn "$PKG: can't release lock on $fh\n";
   }
   return 1; 
}

sub reload { 
   my $self = shift; 
   $self->load($self->{'loaded'});
}
         
sub add {
   my $self = shift; 
   my %args = @_; 
   warn "$PKG: sub add, read: $args{'-read'}, unit: $args{'-unit'}\n";  
   $self->set($args{'-read'}, $args{'-unit'}); 
   
}

sub check {
   my $self = shift; 
   my $want = shift; 
   return $self->{$want} ;
}

sub store { 
   my $self = shift; 
   my $file = shift; 
   my $fh = gensym(); 
   my $output = CDK::ThreadableObject->new(); 
   
   foreach my $v (values %{$self}){
      next if $v=~/ARRAY/i; #we don't want. 
      $output->set($v,[]); 
      #@output{values %{$self}} = undef;
   }
   
   foreach my $k (keys %{$self}){
      next if $k eq 'loaded'; #we don't want this one
      warn "$PKG: sub store, key $k, val $self->{$k}\n";
      $output->set($self->{$k}, [ @{$output->{ $self->{$k} }},$k]);
      #push @{ $output->{$self->{$k}} }, $k; 
   }
   $self->reload();  
   open ($fh, ">$file") or die "$PKG: $!\n"; 
   flock($fh, LOCK_EX) or warn "$PKG: potentially corrupting file in write attempt, unable to secure exclusive lock\n";

   foreach my $k (sort keys %{$output}){
      print $fh "$k";
      foreach my $seq (@{ $output->{$k} }){
         print $fh "\t$seq";
      }
      print $fh "\n";
   }
   flock($fh, LOCK_UN) or warn "$PKG: potentially corrupted file, unable to unlock exclusive lock\n";

}

sub count {
   my $self = shift; 
   return scalar keys %{$self};
}


1; # it true ! 
