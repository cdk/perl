package CDK::WigReader;

use strict;
use Symbol qw/gensym/; 
our @ISA = qw(Exporter);
our $VERSION = '0.01';

#header is key="value of key" key=value,value,value key=value key="value of key"
#refseqid   start   end value

sub new { 
   my $self = {}; 
   my $that = shift;  
   $pkg = ref($that) ||$that; 
   bless $self, $pkg; 
   if (@_){ 
     my $file = shift; 
     $self->setFile($file); 
   }
   return $self; 
} 
sub setFile ($){ 
    my $self = shift; 
    my $file = shift; 
    $self->{'File'} = $file; 
} 
sub parse { 
    my $self = shift;
    my $data;  
    my $fh = gensym; 
    my $file = $self->getFile(); 
    open($fh, $file) or die "$!\n"; 
    my $header = <$fh>; 
    $self->parseHeader($header); 
    while(<$fh>){ 
        my($id, $gp1, $gp2, $val) = split "\t" ; 
        $data->{$id}->[$gp1..$gp2] = map {$value} ($gp1..$gp2); 
    }
    $self->{'Data'} = $data; 
    return; 
} 
sub getHeader { 
    my $self = shift; 
    #returns CDK::WigFileHeader object
    return $self->{'Header'}; 
} 
sub getData { 
    my $self = shift; 
    return wantarray ? %{ $self->{'Data'} } : $self->{'Data'} ; 
} 

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

CDK::WigReader - Perl extension for blah blah blah

=head1 SYNOPSIS

  use CDK::WigReader;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for CDK::WigReader, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

cameron, E<lt>cameron@apple.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by cameron

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.0 or,
at your option, any later version of Perl 5 you may have available.


=cut
